// Copyright 2020 Rob Muhlestein.
// Use of this source code is governed by the highly permissive Apache
// 2.0 license that can be found in the LICENSE file.

package pegn_test

import (
	"fmt"

	pegn "gitlab.com/pegn/go"
	"gitlab.com/pegn/go/ast/nd"
)

func ExampleNode_JSON() {

	n := new(pegn.Node)
	n.Print()
	n.Value = "lit\neral"
	n.Print()
	n.AppendChild(new(pegn.Node))
	n.Print()
	n.FirstChild.AppendChild(new(pegn.Node))
	n.Print()

	// Output:
	// []
	// [0,"lit\neral"]
	// [0,[[]]]
	// [0,[[0,[[]]]]]

}

func ExampleNode_new() {

	// empty and unknown
	zer := new(pegn.Node)
	zer.Print()

	// literal node
	lit := new(pegn.Node)
	lit.Type = 3
	lit.Value = "val"
	lit.Print()

	// container node
	con := new(pegn.Node)
	con.Type = nd.Grammar
	con.Value = "ignored"
	con.AppendChild(lit)
	con.Print()

	// container but no type
	zer.AppendChild(lit)
	zer.Print()

	// Output:
	// []
	// [3,"val"]
	// [1,[[3,"val"]]]
	// [0,[[3,"val"]]]
}

func ExampleNode_AppendChild() {

	const (
		_ = iota
		Mum
		Kid
	)

	// Value later ignored because container
	mum := new(pegn.Node)
	mum.Value = "mummy"
	mum.Type = Mum
	mum.Print()            // [1,"mummy"]
	mum.FirstChild.Print() // <nil>

	kid := new(pegn.Node)
	kid.Type = Kid
	kid.Value = "kid"
	mum.AppendChild(kid) // mum now container

	kid.Print()            // [2,"kid"]
	mum.FirstChild.Print() // [2,"kid"]
	mum.LastChild.Print()  // [2,"kid"]
	kid.Parent.Print()     // [1,[[2,"kid"]]]

	another := new(pegn.Node)
	another.Type = Kid
	another.Value = "another"
	mum.AppendChild(another)
	another.Parent.Print()  // [1,[[2,"kid"],[2,"another"]]]
	mum.LastChild.Print()   // [2,"another"]
	kid.NextSib.Print()     // [2,"another"]
	another.PrevSib.Print() // [2,"kid"]

	// Output:
	// [1,"mummy"]
	// <nil>
	// [2,"kid"]
	// [2,"kid"]
	// [2,"kid"]
	// [1,[[2,"kid"]]]
	// [1,[[2,"kid"],[2,"another"]]]
	// [2,"another"]
	// [2,"another"]
	// [2,"kid"]
}

func ExampleNode_AdoptFrom() {

	const (
		_ = iota
		Parent
		Child
	)

	mum := new(pegn.Node)
	mum.Type = Parent

	kid := new(pegn.Node)
	kid.Type = Child
	kid.Value = "kid"
	mum.AppendChild(kid)

	another := new(pegn.Node)
	another.Type = Child
	another.Value = "another"
	mum.AppendChild(another)

	unc := new(pegn.Node)
	unc.Type = Parent

	own := new(pegn.Node)
	own.Type = Child
	own.Value = "own"
	unc.AppendChild(own)

	mum.Print()
	unc.Print()
	unc.AdoptFrom(mum)
	mum.Print()
	unc.Print()

	// Output:
	// [1,[[2,"kid"],[2,"another"]]]
	// [1,[[2,"own"]]]
	// [1]
	// [1,[[2,"own"],[2,"kid"],[2,"another"]]]

}

func ExampleNode_InsertBeforeSelf_noparent() {

	// no parent is fine (but difficult later add parents)
	sib := new(pegn.Node)
	sib.Value = "sib"
	another := new(pegn.Node)
	another.Value = "another"
	sib.InsertBeforeSelf(another)
	sib.PrevSib.Print()         // [0,"another"]
	another.NextSib.Print()     // [0,"sib"]
	fmt.Println(sib.Parent)     // <nil>
	fmt.Println(another.Parent) // <nil>

	// [0,"another"]
	// [0,"sib"]
	// <nil>
	// <nil>

}

func ExampleNode_InsertBeforeSelf_parent() {

	// best to start with parent
	mum := new(pegn.Node)
	sib := new(pegn.Node)
	sib.Value = "sib"
	mum.AppendChild(sib)
	another := new(pegn.Node)
	another.Value = "another"
	sib.InsertBeforeSelf(another)
	sib.PrevSib.Print()     // [0,"another"]
	another.NextSib.Print() // [0,"sib"]
	sib.Parent.Print()      // [0,[[0,"another"],[0,"sib"]]]
	another.Parent.Print()  // [0,[[0,"another"],[0,"sib"]]]
	mum.FirstChild.Print()  // [0,"another"]
	mum.LastChild.Print()   // [0,"sib"]

	// Output:
	// [0,"another"]
	// [0,"sib"]
	// [0,[[0,"another"],[0,"sib"]]]
	// [0,[[0,"another"],[0,"sib"]]]
	// [0,"another"]
	// [0,"sib"]

}

func ExampleNode_AppendAfterSelf() {

	mum := new(pegn.Node)
	one := new(pegn.Node)
	two := new(pegn.Node)
	one.Value = "one"
	two.Value = "two"
	mum.AppendChild(one)
	one.AppendAfterSelf(two)
	mum.Print()

	// Output:
	// [0,[[0,"one"],[0,"two"]]]
}

func ExampleNode_RemoveSelf_first() {

	mum := new(pegn.Node)
	one := new(pegn.Node)
	two := new(pegn.Node)
	three := new(pegn.Node)

	two.Value = "two"
	three.Value = "three"

	mum.AppendChild(one)
	mum.AppendChild(two)
	mum.AppendChild(three)
	mum.Print()

	gr := new(pegn.Node)
	gr.Value = "grandchild"
	one.AppendChild(gr)

	orphan := one.RemoveSelf()
	orphan.Print()
	mum.Print()
	fmt.Println(two.PrevSib)
	mum.FirstChild.Print()
	mum.LastChild.Print()

	// Output:
	// [0,[[],[0,"two"],[0,"three"]]]
	// [0,[[0,"grandchild"]]]
	// [0,[[0,"two"],[0,"three"]]]
	// <nil>
	// [0,"two"]
	// [0,"three"]

}

func ExampleNode_RemoveSelf_middle() {

	mum := new(pegn.Node)
	one := new(pegn.Node)
	two := new(pegn.Node)
	three := new(pegn.Node)

	one.Value = "one"
	two.Value = "two"
	three.Value = "three"

	mum.AppendChild(one)
	mum.AppendChild(two)
	mum.AppendChild(three)
	mum.Print()

	orphan := two.RemoveSelf()
	orphan.Print()
	mum.Print()
	one.NextSib.Print()
	three.PrevSib.Print()
	mum.FirstChild.Print()
	mum.LastChild.Print()

	// Output:
	// [0,[[0,"one"],[0,"two"],[0,"three"]]]
	// [0,"two"]
	// [0,[[0,"one"],[0,"three"]]]
	// [0,"three"]
	// [0,"one"]
	// [0,"one"]
	// [0,"three"]

}

func ExampleNode_RemoveSelf_last() {

	mum := new(pegn.Node)
	one := new(pegn.Node)
	two := new(pegn.Node)
	three := new(pegn.Node)

	mum.Value = "mummy"
	one.Value = "one"
	two.Value = "two"
	three.Value = "three"

	mum.AppendChild(one)
	mum.AppendChild(two)
	mum.AppendChild(three)
	mum.Print()

	orphan := three.RemoveSelf()
	orphan.Print()
	mum.Print()
	one.NextSib.Print()
	fmt.Println(two.NextSib)
	mum.LastChild.Print()

	// Output:
	// [0,[[0,"one"],[0,"two"],[0,"three"]]]
	// [0,"three"]
	// [0,[[0,"one"],[0,"two"]]]
	// [0,"two"]
	// <nil>
	// [0,"two"]

}

func ExampleNode_ReplaceSelf_first() {

	mum := new(pegn.Node)
	one := new(pegn.Node)
	two := new(pegn.Node)
	three := new(pegn.Node)

	two.Value = "two"
	three.Value = "three"

	mum.AppendChild(one)
	mum.AppendChild(two)
	mum.AppendChild(three)

	gr := new(pegn.Node)
	gr.Value = "grandchild"
	one.AppendChild(gr)

	mum.Print()

	nw := new(pegn.Node)
	nw.Value = "replacement"

	orphan := one.ReplaceSelf(nw)
	orphan.Print()

	mum.Print()
	mum.FirstChild.Print()
	mum.LastChild.Print()

	fmt.Println(two.PrevSib)
	fmt.Println(nw.NextSib)
	fmt.Println(nw.PrevSib)

	// Output:
	// [0,[[0,[[0,"grandchild"]]],[0,"two"],[0,"three"]]]
	// [0,[[0,"grandchild"]]]
	// [0,[[0,"replacement"],[0,"two"],[0,"three"]]]
	// [0,"replacement"]
	// [0,"three"]
	// [0,"replacement"]
	// [0,"two"]
	// <nil>

}

func ExampleNode_ReplaceSelf_middle() {

	mum := new(pegn.Node)
	one := new(pegn.Node)
	two := new(pegn.Node)
	three := new(pegn.Node)

	one.Value = "one"
	three.Value = "three"

	mum.AppendChild(one)
	mum.AppendChild(two)
	mum.AppendChild(three)

	gr := new(pegn.Node)
	gr.Value = "grandchild"
	two.AppendChild(gr)

	mum.Print()

	nw := new(pegn.Node)
	nw.Value = "replacement"

	orphan := two.ReplaceSelf(nw)
	orphan.Print()

	mum.Print()
	mum.FirstChild.Print()
	mum.LastChild.Print()

	fmt.Println(one.NextSib)
	fmt.Println(nw.PrevSib)
	fmt.Println(nw.NextSib)
	fmt.Println(three.PrevSib)

	// Output:
	// [0,[[0,"one"],[0,[[0,"grandchild"]]],[0,"three"]]]
	// [0,[[0,"grandchild"]]]
	// [0,[[0,"one"],[0,"replacement"],[0,"three"]]]
	// [0,"one"]
	// [0,"three"]
	// [0,"replacement"]
	// [0,"one"]
	// [0,"three"]
	// [0,"replacement"]

}

func ExampleNode_ReplaceSelf_last() {

	mum := new(pegn.Node)
	one := new(pegn.Node)
	two := new(pegn.Node)
	three := new(pegn.Node)

	one.Value = "one"
	two.Value = "two"

	mum.AppendChild(one)
	mum.AppendChild(two)
	mum.AppendChild(three)

	gr := new(pegn.Node)
	gr.Value = "grandchild"
	three.AppendChild(gr)

	mum.Print()

	nw := new(pegn.Node)
	nw.Value = "replacement"

	orphan := three.ReplaceSelf(nw)
	orphan.Print()

	mum.Print()
	mum.FirstChild.Print()
	mum.LastChild.Print()

	fmt.Println(two.NextSib)
	fmt.Println(nw.PrevSib)
	fmt.Println(nw.NextSib)

	// Output:
	// [0,[[0,"one"],[0,"two"],[0,[[0,"grandchild"]]]]]
	// [0,[[0,"grandchild"]]]
	// [0,[[0,"one"],[0,"two"],[0,"replacement"]]]
	// [0,"one"]
	// [0,"replacement"]
	// [0,"replacement"]
	// [0,"two"]
	// <nil>

}

func ExampleNode_Children() {
	mum := new(pegn.Node)
	one := new(pegn.Node)
	two := new(pegn.Node)
	three := new(pegn.Node)

	one.Value = "one"
	two.Value = "two"
	three.Value = "three"

	fmt.Println(mum.Children())

	mum.AppendChild(one)
	mum.AppendChild(two)
	mum.AppendChild(three)

	fmt.Println(mum.Children())

	// Output:
	// []
	// [[0,"one"] [0,"two"] [0,"three"]]
}

func ExampleNode_Visit() {

	const (
		_ = iota
		EndOfLine
		String
		Space

		_punct_start
		Hyphen
		Question
		Exclamation
		_punct_end

		_container_start
		Hyphenated
		_container_end
	)

	newSpace := func() *pegn.Node {
		sp := new(pegn.Node)
		sp.Value = " "
		sp.Type = Space
		return sp
	}

	action := func(n *pegn.Node) interface{} {
		//fmt.Printf("  %v", n.Type())
		if n.Type == EndOfLine {
			//fmt.Print("(removing)")
			if n.Parent.Type == Hyphenated {
				n.RemoveSelf()
				return nil
			}
			n.ReplaceSelf(newSpace())
			return nil
		}
		return nil
	}

	write := func(n *pegn.Node) interface{} {
		fmt.Printf("%v", n.Value)
		return nil
	}

	mum := new(pegn.Node)

	hi := new(pegn.Node)
	hi.Value = "hi"
	hi.Type = String
	mum.AppendChild(hi)
	mum.Print()

	eol := new(pegn.Node)
	eol.Value = "\n"
	eol.Type = EndOfLine
	mum.AppendChild(eol)
	mum.Print()

	there := new(pegn.Node)
	there.Value = "there"
	there.Type = String
	mum.AppendChild(there)
	mum.Print()

	mum.AppendChild(newSpace())
	mum.Print()

	co := new(pegn.Node)
	co.Value = "co"
	co.Type = String

	hy := new(pegn.Node)
	hy.Value = "-"
	hy.Type = Hyphen

	aut := new(pegn.Node)
	aut.Value = "aut"
	aut.Type = String

	eol2 := new(pegn.Node)
	eol2.Value = "\n"
	eol2.Type = EndOfLine

	hor := new(pegn.Node)
	hor.Value = "hor"
	hor.Type = String

	coauth := new(pegn.Node)
	// Hyphenated <-- (alphanum / EndOfLine) DASH (alphanum / EndOfLine)
	// Hyphenated <-- alphanum DASH alphanum
	// EndOfLine  <-- CR LF / CR / LF
	coauth.Type = Hyphenated
	coauth.AppendChild(co)
	coauth.AppendChild(hy)
	coauth.AppendChild(aut)
	coauth.AppendChild(eol2)
	coauth.AppendChild(hor)
	mum.AppendChild(coauth)

	eol3 := new(pegn.Node)
	eol3.Value = "\n"
	eol3.Type = EndOfLine
	mum.AppendChild(eol3)

	mum.Print()
	mum.Visit(write, nil)
	mum.Visit(action, nil)
	mum.Print()
	mum.Visit(write, nil)

	// Output:
	// [0,[[2,"hi"]]]
	// [0,[[2,"hi"],[1,"\n"]]]
	// [0,[[2,"hi"],[1,"\n"],[2,"there"]]]
	// [0,[[2,"hi"],[1,"\n"],[2,"there"],[3," "]]]
	// [0,[[2,"hi"],[1,"\n"],[2,"there"],[3," "],[10,[[2,"co"],[5,"-"],[2,"aut"],[1,"\n"],[2,"hor"]]],[1,"\n"]]]
	// hi
	// there co-aut
	// hor
	// [0,[[2,"hi"],[3," "],[2,"there"],[3," "],[10,[[2,"co"],[5,"-"],[2,"aut"],[2,"hor"]]],[3," "]]]
	// hi there co-author

}
