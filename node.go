// Copyright 2020 Rob Muhlestein.
// Use of this source code is governed by the highly permissive Apache
// 2.0 license that can be found in the LICENSE file.

package pegn

import (
	"encoding/json"
	"fmt"
)

// Action is a first-class function used when Visiting each Node. The
// return value will be sent to a channel as each Action completed. It
// can be an error or something else.
type Action func(n *Node) interface{}

type Node struct {
	Type       int    // 0 = nd.Undefined
	Value      string // only for literal types (no children)
	Parent     *Node
	PrevSib    *Node
	NextSib    *Node
	FirstChild *Node
	LastChild  *Node
}

func (n *Node) InsertBeforeSelf(c *Node) {
	c.Parent = n.Parent
	if n.PrevSib == nil {
		c.NextSib = n
		n.PrevSib = c
		if n.Parent != nil {
			n.Parent.FirstChild = c
		}
		return
	}
	c.PrevSib = n.PrevSib
	c.NextSib = n
	n.PrevSib.NextSib = c
	n.PrevSib = c
}

func (n *Node) AppendAfterSelf(c *Node) {
	c.Parent = n.Parent
	if n.NextSib == nil {
		c.PrevSib = n
		n.NextSib = c
		if n.Parent != nil {
			n.Parent.LastChild = c
		}
		return
	}
	c.NextSib = n.NextSib
	c.PrevSib = n
	n.NextSib.PrevSib = c
	n.NextSib = c
}

func (n *Node) RemoveSelf() *Node {
	if n.Parent != nil {
		if n.Parent.FirstChild == n {
			n.Parent.FirstChild = n.NextSib
		}
		if n.Parent.LastChild == n {
			n.Parent.LastChild = n.PrevSib
		}
	}
	if n.PrevSib != nil {
		n.PrevSib.NextSib = n.NextSib
	}
	if n.NextSib != nil {
		n.NextSib.PrevSib = n.PrevSib
	}
	n.Parent = nil
	n.NextSib = nil
	n.PrevSib = nil
	return n
}

func (n *Node) ReplaceSelf(c *Node) *Node {
	c.Parent = n.Parent
	c.PrevSib = n.PrevSib
	c.NextSib = n.NextSib
	if n.Parent.LastChild == n {
		n.Parent.LastChild = c
	}
	if n.Parent.FirstChild == n {
		n.Parent.FirstChild = c
	}
	if n.PrevSib != nil {
		n.PrevSib.NextSib = c
	}
	if n.NextSib != nil {
		n.NextSib.PrevSib = c
	}
	n.Parent = nil
	n.NextSib = nil
	n.PrevSib = nil
	return n
}

func (n *Node) Children() []*Node {
	if n.FirstChild == nil {
		return nil
	}
	cur := n.FirstChild
	c := []*Node{cur}
	for {
		cur = cur.NextSib
		if cur == nil {
			break
		}
		c = append(c, cur)
	}
	return c
}

func (n *Node) AppendChild(c *Node) {
	if n.FirstChild == nil {
		c.Parent = n
		n.FirstChild = c
		n.LastChild = c
		return
	}
	n.LastChild.AppendAfterSelf(c)
}

func (n *Node) AdoptFrom(other *Node) {
	if other.FirstChild == nil {
		return
	}
	c := other.FirstChild.RemoveSelf()
	n.AppendChild(c)
	n.AdoptFrom(other)
}

func (n *Node) Visit(a Action, rvals chan interface{}) {
	if rvals == nil {
		a(n)
	} else {
		rvals <- a(n)
	}
	if n.FirstChild == nil {
		return
	}
	for _, c := range n.Children() {
		c.Visit(a, rvals)
	}
	return
}

/*
TODO
func (n *node) VisitAsync(a Action, rvals chan interface{}) {
	return
}
*/

func (n *Node) JSON() string {
	b, _ := n.MarshalJSON()
	if b == nil {
		return "[null]"
	}
	return string(b)
}

// MarshalJSON fulfills the interface and avoids use of slower
// reflection parsing from using struct tagging. This also allows
// detection of the second argument as either a string literal or a tree
// of child Nodes (but not both).
func (n *Node) MarshalJSON() ([]byte, error) {
	ch := n.Children()
	if len(ch) == 0 {
		if n.Value == "" {
			if n.Type == 0 {
				return []byte("[]"), nil
			}
			return []byte(fmt.Sprintf(`[%d]`, n.Type)), nil
		}
		return []byte(fmt.Sprintf(`[%d,%q]`, n.Type, n.Value)), nil
	}
	chs, err := json.Marshal(ch)
	if err != nil {
		return nil, err
	}
	return []byte(fmt.Sprintf(`[%d,%v]`, n.Type, string(chs))), nil
}

func (n *Node) UnmarshalJSON(b []byte) error {
	u := []interface{}{}
	err := json.Unmarshal(b, &u)
	if err != nil {
		return err
	}
	// empty array ([]) is simplest node type, assume nd.Unknown
	if len(u) == 0 {
		return nil
	}
	// everything has a type integer as first item in list
	n.Type = int(u[0].(float64))
	if len(u) == 1 { // [2]
		return nil
	}
	// nodes are either containers or literals, never both
	switch v := u[1].(type) {
	case string: // [2,"some"]
		n.Value = v
	case [][]byte: // [2, [[3],[],[2,"some"],[4,[[3]]]]]
		n.FirstChild = new(Node)
		err = n.FirstChild.UnmarshalJSON(v[0])
		if err != nil {
			return err
		}
		cur := n.FirstChild
		for _, m := range v[1:] {
			c := new(Node)
			err := c.UnmarshalJSON(m)
			if err != nil {
				return nil
			}
			cur.AppendAfterSelf(c)
			cur = c
		}
		n.LastChild = cur
	}
	return nil
}

func (n *Node) String() string { return n.JSON() }
func (n *Node) Print()         { fmt.Println(n) }
