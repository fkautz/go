package parser

import (
	"fmt"
	"io"
	"io/ioutil"
	"unicode/utf8"

	pegn "gitlab.com/pegn/go"
	"gitlab.com/pegn/go/is"
)

// New returns an instance of the default pegn.Parser implementation
// and is used by the functions from the ast and pf packages. Must call
// parser.Init() before using.
func New() pegn.Parser { return new(parser) }

type parser struct {
	in    io.Reader
	buf   []byte
	mk    *pegn.Mark
	endmk *pegn.Mark
}

func (p *parser) bufferInput(i interface{}) error {
	var err error
	switch in := i.(type) {
	case io.Reader:
		p.buf, err = ioutil.ReadAll(in)
		if err != nil {
			return err
		}
	case string:
		p.buf = []byte(in)
	case []byte:
		p.buf = in
	default:
		return fmt.Errorf("Unsupported input type: %t", i)
	}
	if len(p.buf) == 0 {
		return fmt.Errorf("Empty input")
	}
	return err
}

func (p *parser) Peek() rune {
	rn, _ := utf8.DecodeRune(p.buf) // scan first
	return rn
}

func (p *parser) Init(i interface{}) error {
	if err := p.bufferInput(i); err != nil {
		return err
	}
	cur, ln := utf8.DecodeRune(p.buf) // scan first
	if ln == 0 {
		return fmt.Errorf("Failed to scan first rune")
	}
	p.mk = new(pegn.Mark)
	p.mk.Rune = cur
	p.mk.Len = ln
	p.mk.Off.Byte = 1
	p.mk.Off.Rune = 1
	p.mk.Next = ln
	p.mk.Pos.Rune = 1
	p.mk.Pos.Byte = 1
	p.mk.Pos.Line = 1
	return nil
}

func (p *parser) Next() {
	if p.endmk == p.mk {
		return
	}
	rn, ln := utf8.DecodeRune(p.buf[p.mk.Next:])
	if rn == utf8.RuneError && ln == 0 {
		p.endmk = p.mk
	}
	p.mk.Rune = rn
	p.mk.Len = ln
	// actual offset in data
	p.mk.Next += p.mk.Len
	// user land
	p.mk.Off.Byte += p.mk.Len
	p.mk.Off.Rune += 1
	p.mk.Pos.Rune += 1
	p.mk.Pos.Byte += p.mk.Len
}

func (p *parser) Move(n int) {
	for i := 0; i < n; i++ {
		p.Next()
	}
}

func (p *parser) Done() bool { return p.mk == p.endmk }

func (p *parser) String() string { return p.mk.String() }
func (p *parser) Print()         { fmt.Println(p) }

func (p *parser) Mark() *pegn.Mark {
	if p.mk == nil {
		return nil
	}
	// force a copy
	cp := *p.mk
	return &cp
}

func (p *parser) Goto(m *pegn.Mark) { nm := *m; p.mk = &nm }

func (p *parser) NewLine() {
	p.mk.Pos.Rune = 1
	p.mk.Pos.Byte = 1
	p.mk.Pos.Line++
}

func (p *parser) Parse(m *pegn.Mark) string {
	if m.Off.Byte < p.mk.Off.Byte {
		return string(p.buf[m.Off.Byte-1 : p.mk.Next])
	}
	return string(p.buf[p.mk.Off.Byte-1 : m.Next])
}

func (p *parser) Slice(beg *pegn.Mark, end *pegn.Mark) string {
	return string(p.buf[beg.Off.Byte-1 : end.Next])
}

func (p *parser) Expect(ms ...interface{}) *pegn.Mark {
	var beg, end *pegn.Mark
	beg = p.Mark()
	for _, m := range ms {
		switch v := m.(type) {

		case rune:
			if p.mk.Rune != v {
				p.Goto(beg)
				return nil
			}
			end = p.Mark()
			p.Next()

		case string:
			for _, r := range []rune(v) {
				if p.mk.Rune != r {
					p.Goto(beg)
					return nil
				}
				end = p.Mark()
				p.Next()
			}

		case pegn.IsFunc, func(r int32) bool:
			if !v.(func(r int32) bool)(p.mk.Rune) {
				p.Goto(beg)
				return nil
			}
			end = p.Mark()
			p.Next()

		case pegn.CheckFunc, func(p pegn.Parser) *pegn.Mark:
			rv := v.(func(p pegn.Parser) *pegn.Mark)(p)
			if rv == nil {
				p.Goto(beg)
				return nil
			}
			end = rv
			p.Goto(rv)
			p.Next()

		case is.Not:
			if m := p.Check(v.This); m != nil {
				p.Goto(beg)
				return nil
			}
			end = p.Mark()

		case is.Min:
			c := 0
			last := p.Mark()
			for {
				m := p.Expect(v.Match)
				if m == nil {
					break
				}
				last = m
				c++
			}
			if c < v.Min {
				p.Goto(beg)
				return nil
			}
			end = last

		case is.Count:
			m := p.Expect(is.MinMax{v.Match, v.Count, v.Count})
			if m == nil {
				p.Goto(beg)
				return nil
			}
			end = m

		case is.MinMax:
			c := 0
			last := p.Mark()
			for {
				m := p.Expect(v.Match)
				if m == nil {
					break
				}
				last = m
				c++
			}
			if !(v.Min <= c && c <= v.Max) {
				p.Goto(beg)
				return nil
			}
			end = last

		case is.Seq:
			m := p.Expect(v...)
			if m == nil {
				p.Goto(beg)
				return nil
			}
			end = m

		default:
			panic("Invalid Check()/Expect() type")
		}
	}
	return end
}

func (p *parser) Error(typ int, args ...interface{}) {
	// TODO
}

func (p *parser) Check(ms ...interface{}) *pegn.Mark {
	defer p.Goto(p.Mark())
	return p.Expect(ms...)
}
