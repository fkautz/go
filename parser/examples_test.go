package parser_test

import (
	"fmt"

	pegn "gitlab.com/pegn/go"
	"gitlab.com/pegn/go/is"
	"gitlab.com/pegn/go/parser"
)

func ExampleNew() {

	p := parser.New()
	fmt.Println(p.Mark())

	// Output:
	// <nil>

}

func Example_parser_Init() {

	p := parser.New()
	p.Init("something")
	p.Print()
	err := p.Init("")
	fmt.Println(err)
	p.Init("b")
	p.Print()
	p.Next()
	p.Print()
	fmt.Println(p.Done())

	// Output:
	// U+0073 's' 1,1-1 (1-1)
	// Empty input
	// U+0062 'b' 1,1-1 (1-1)
	// U+FFFD '�' 1,2-1 (2-1)
	// true

}

func Example_parser_Next() {

	p := parser.New()
	s := "so\U0001F47Fmething here\nsomething there"
	fmt.Println(len(s))
	fmt.Println(len([]rune(s)))
	p.Init(s)
	p.Print()
	p.Next()
	p.Print()
	p.Next()
	p.Print()

	// Output:
	// 34
	// 31
	// U+0073 's' 1,1-1 (1-1)
	// U+006F 'o' 1,2-2 (2-2)
	// U+1F47F '👿' 1,3-6 (3-6)

}

func Example_parser_Parse_first() {
	p := parser.New()
	p.Init("something")
	p.Print()
	m := p.Mark()
	fmt.Println(p.Parse(m) == "s")
	// Output:
	// U+0073 's' 1,1-1 (1-1)
	// true
}

func Example_parser_Parse_something() {

	p := parser.New()
	p.Init("something")
	p.Print()
	m := p.Mark()
	p.Next()
	p.Next()
	p.Next()
	p.Print()
	fmt.Println(p.Parse(m))

	m2 := p.Mark()
	p.Goto(m)
	p.Print()
	fmt.Println(p.Parse(m2))

	// Output:
	// U+0073 's' 1,1-1 (1-1)
	// U+0065 'e' 1,4-4 (4-4)
	// some
	// U+0073 's' 1,1-1 (1-1)
	// some

}

func Example_parser_Slice() {

	p := parser.New()
	p.Init("something")
	p.Print()
	p.Move(3)
	p.Print()
	b := p.Mark()
	p.Move(2)
	p.Print()
	e := p.Mark()
	fmt.Println(p.Slice(b, e))

	// Output:
	// U+0073 's' 1,1-1 (1-1)
	// U+0065 'e' 1,4-4 (4-4)
	// U+0068 'h' 1,6-6 (6-6)
	// eth

}

func Example_parser_Expect_rune() {
	p := parser.New()
	p.Init("something")
	p.Expect('s').Print()
	p.Print()
	p.Expect('o').Print()
	p.Print()
	p.Move(2)
	p.Print()
	p.Expect('t').Print()
	p.Print()
	p.Move(3)
	p.Expect('g').Print()
	p.Print()
	// Output:
	// U+0073 's' 1,1-1 (1-1)
	// U+006F 'o' 1,2-2 (2-2)
	// U+006F 'o' 1,2-2 (2-2)
	// U+006D 'm' 1,3-3 (3-3)
	// U+0074 't' 1,5-5 (5-5)
	// U+0074 't' 1,5-5 (5-5)
	// U+0068 'h' 1,6-6 (6-6)
	// U+0067 'g' 1,9-9 (9-9)
	// U+FFFD '�' 1,10-9 (10-9)
}

func Example_parser_Expect_string() {
	p := parser.New()
	p.Init("something")
	p.Expect("some").Print()
	p.Expect("thing").Print()
	// Output:
	// U+0065 'e' 1,4-4 (4-4)
	// U+0067 'g' 1,9-9 (9-9)
}

func Example_parser_Expect_isfunc() {
	p := parser.New()
	p.Init("something")
	p.Move(3)
	is_e := func(r rune) bool { return r == 'e' }
	p.Expect(is_e).Print()
	p.Expect(is.Alpha).Print()
	// Output:
	// U+0065 'e' 1,4-4 (4-4)
	// U+0074 't' 1,5-5 (5-5)
}

func Example_parser_Expect_checkfunc() {
	p := parser.New()
	p.Init("something")
	p.Move(3)
	p.Print() // U+0065 'e' 1,4-4 (4-4)

	ck_e := func(p pegn.Parser) *pegn.Mark {
		m := p.Mark()
		if m.Rune != 'e' {
			return nil
		}
		return m
	}
	ck_t := func(p pegn.Parser) *pegn.Mark {
		return p.Check('t')
	}
	ck_hing := func(p pegn.Parser) *pegn.Mark {
		return p.Check("hing")
	}

	p.Expect(ck_e).Print()    // U+0065 'e' 1,4-4 (4-4)
	p.Print()                 // U+0074 't' 1,5-5 (5-5)
	p.Expect(ck_t).Print()    // U+0074 't' 1,5-5 (5-5)
	p.Expect(ck_hing).Print() // U+0067 'g' 1,9-9 (9-9)

	p.Init("     some")
	p.Expect(is.Min{' ', 1}).Print() // U+0020 ' ' 1,5-5 (5-5)
	p.Print()

	// Output:
	// U+0065 'e' 1,4-4 (4-4)
	// U+0065 'e' 1,4-4 (4-4)
	// U+0074 't' 1,5-5 (5-5)
	// U+0074 't' 1,5-5 (5-5)
	// U+0067 'g' 1,9-9 (9-9)
	// U+0020 ' ' 1,5-5 (5-5)
	// U+0073 's' 1,6-6 (6-6)
}

func Example_parser_Expect_not() {
	p := parser.New()
	p.Init("something")
	p.Expect(is.Not{'s'}).Print()
	p.Expect(is.Not{'z'}).Print()
	p.Next()
	p.Expect(is.Not{'0'}).Print()
	// Output:
	// <nil>
	// U+0073 's' 1,1-1 (1-1)
	// U+006F 'o' 1,2-2 (2-2)
}

func Example_parser_Expect_min() {
	p := parser.New()
	p.Init("   something")
	p.Expect(is.Min{' ', 2}).Print() // consume spaces
	p.Print()                        // parser on first after spaces
	p.Expect(is.Min{'z', 0}).Print() // optional z, so no advance
	p.Print()                        // no move in the parser
	// Output:
	// U+0020 ' ' 1,3-3 (3-3)
	// U+0073 's' 1,4-4 (4-4)
	// U+0073 's' 1,4-4 (4-4)
	// U+0073 's' 1,4-4 (4-4)
}

func Example_parser_Expect_minmax() {
	p := parser.New()
	p.Init("   something")
	p.Expect(is.MinMax{' ', 2, 4}).Print()       // consume spaces
	p.Print()                                    // parser on first after spaces
	p.Expect(is.MinMax{'z', 0, 2}).Print()       // optional z, so no advance
	p.Print()                                    // no move in the parser
	p.Expect(is.MinMax{is.Alpha, 0, 10}).Print() // consume rest
	p.Print()                                    // but parser is out of bounds
	p.Init("####")
	p.Expect(is.MinMax{'#', 2, 6}).Print()
	// Output:
	// U+0020 ' ' 1,3-3 (3-3)
	// U+0073 's' 1,4-4 (4-4)
	// U+0073 's' 1,4-4 (4-4)
	// U+0073 's' 1,4-4 (4-4)
	// U+0067 'g' 1,12-12 (12-12)
	// U+FFFD '�' 1,13-12 (13-12)
	// U+0023 '#' 1,4-4 (4-4)
}

func Example_parser_Expect_count() {
	p := parser.New()
	p.Init("####")
	p.Expect(is.Count{'#', 4}).Print()
	// Output:
	// U+0023 '#' 1,4-4 (4-4)
}

func Example_parser_Expect_set() {
	p := parser.New()
	p.Init("####------")
	p.Expect(is.Seq{is.Min{'#', 1}, is.Min{'-', 1}}).Print()
	// Output:
	// U+002D '-' 1,10-10 (10-10)
}

func Example_parser_Expect_compound() {
	p := parser.New()
	p.Init("   U+002D")
	p.Expect(is.Min{' ', 0}, "U+", is.MinMax{is.UpperHex, 4, 8}).Print()
	p.Print()
	// Output:
	// U+0044 'D' 1,9-9 (9-9)
	// U+FFFD '�' 1,10-9 (10-9)
}

func Example_parser_Check() {
	p := parser.New()
	p.Init("    something")
	p.Print()                        // U+0020 ' ' 1,1-1 (1-1)
	p.Check(is.Min{' ', 1}).Print()  // U+0020 ' ' 1,4-4 (4-4)
	p.Print()                        // (didn't advance)
	fmt.Println(p.Check('\t'))       // <nil>
	p.Print()                        // (still not advanced)
	p.Check(is.Min{'\t', 0}).Print() // (true but not advanced)
	p.Init("-")
	p.Print()
	p.Check('-').Print()

	// Output:
	// U+0020 ' ' 1,1-1 (1-1)
	// U+0020 ' ' 1,4-4 (4-4)
	// U+0020 ' ' 1,1-1 (1-1)
	// <nil>
	// U+0020 ' ' 1,1-1 (1-1)
	// U+0020 ' ' 1,1-1 (1-1)
	// U+002D '-' 1,1-1 (1-1)
	// U+002D '-' 1,1-1 (1-1)
}
