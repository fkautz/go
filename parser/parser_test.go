package parser

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestParser_bufferInput(t *testing.T) {
	p := new(parser)
	var e error
	// byte array is used directly
	e = p.bufferInput([]byte("something"))
	require.Equal(t, []byte("something"), p.buf)
	require.Nil(t, e)
	// string cast to byte array
	e = p.bufferInput("something")
	require.Equal(t, []byte("something"), p.buf)
	require.Nil(t, e)
	// io.Reader fully loaded
	e = p.bufferInput(strings.NewReader("something"))
	require.Equal(t, []byte("something"), p.buf)
	require.Nil(t, e)
	// single rune fails but does not affect current buffer
	e = p.bufferInput('d')
	require.NotNil(t, e)
	require.Equal(t, []byte("something"), p.buf)
	// integer fails but does not affect current buffer
	e = p.bufferInput(34)
	require.NotNil(t, e)
	require.Equal(t, []byte("something"), p.buf)
}

func TestParser_Init(t *testing.T) {
	p := new(parser)
	require.Nil(t, p.mk)
	p.Init("something")
	require.NotNil(t, p.mk)
	// should have read the first rune of first line
	require.Equal(t, 's', p.mk.Rune)
	require.Equal(t, 1, p.mk.Len)
	require.Equal(t, 1, p.mk.Next) // first was 0
	require.Equal(t, 1, p.mk.Off.Byte)
	require.Equal(t, 1, p.mk.Off.Rune)
	require.Equal(t, 1, p.mk.Pos.Line)
	require.Equal(t, 1, p.mk.Pos.Rune)
	require.Equal(t, 1, p.mk.Pos.Byte)
	require.Equal(t, p.Mark(), p.mk)
}
