// Copyright 2020 Rob Muhlestein.
// Use of this source code is governed by the highly permissive Apache
// 2.0 license that can be found in the LICENSE file.

/*
Package parse contains many simple boolean pegn.Filter functions that
match true when a given rune matches one of the Unicode code points
specified in the description. These are suitable for passing directly
to pegn.Consume(). See the PEGN specification for details.
*/
package is

import "gitlab.com/pegn/go/tk"

type Min struct {
	Match interface{}
	Min   int
}

type Count struct {
	Match interface{}
	Count int
}

type Seq []interface{}

type MinMax struct {
	Match interface{}
	Min   int
	Max   int
}

type Not struct {
	This interface{}
}

// Any returns true if rune is any Unicode codepoint [U+0000-U+10FFFF]
// except RuneError.
func Any(r rune) bool { return '\u0000' <= r && r <= '\U0010FFFF' && r != tk.UNKNOWN }

// Upper returns true if rune is any ASCII uppercase letter.
func Upper(r rune) bool { return 'A' <= r && r <= 'Z' }

// Lower returns true if rune is any ASCII lowercase letter.
func Lower(r rune) bool { return 'a' <= r && r <= 'z' }

// Alpha returns true if rune is any ASCII letter.
func Alpha(r rune) bool { return Lower(r) || Upper(r) }

// Digit returns true if rune is an ASCII digit.
func Digit(r rune) bool { return '0' <= r && r <= '9' }

// Quotable returns true if rune is an alphanum,  space, bang or falls
// within the following ASCII/Unicode ranges [U+0023-U+002F],
// [U+003A-U+0040], [U+005B-U+0060], or [U+007B-U+007E].
func Quotable(r rune) bool {
	switch {
	case
		AlphaNum(r),
		r == ' ', r == '!',
		'\u0023' <= r && r <= '\u002F',
		'\u003A' <= r && r <= '\u0040',
		'\u005B' <= r && r <= '\u0060',
		'\u007B' <= r && r <= '\u007E':
		return true
	default:
		return false
	}
}

// Punct returns true if rune falls within the following
// ASCII/Unicode ranges [U+0021-U+002F], [U+003A-U+0040],
// [U+005B-U+0060], or [U+007B-U+007E].
func Punct(r rune) bool {
	switch {
	case
		'\u0021' <= r && r <= '\u002F',
		'\u003A' <= r && r <= '\u0040',
		'\u005B' <= r && r <= '\u0060',
		'\u007B' <= r && r <= '\u007E':
		return true
	default:
		return false
	}
}

// AlphaNum returns true if rune is Alpha or Digits. Underscore is not
// included.
func AlphaNum(r rune) bool { return Alpha(r) || Digit(r) }

// Hexdig returns true if rune is LowerHex or UpperHex.
func Hexdig(r rune) bool {
	return Digit(r) || ('a' <= r && r <= 'f') || ('A' <= r && r <= 'F')
}

// Octdig returns true if rune is [0-7]
func Octdig(r rune) bool { return '0' <= r && r <= '7' }

// Bitdig returns true if rune is '0' or '1'
func Bitdig(r rune) bool { return r == '0' || r == '1' }

// LowerHex returns true if rune is digit / [a-f]
func LowerHex(r rune) bool {
	return Digit(r) || 'a' <= r && r <= 'f'
}

// UpperHex returns true if rune is digit / [A-F]
func UpperHex(r rune) bool {
	return Digit(r) || 'A' <= r && r <= 'F'
}

// Visible returns true if rune is a visible ASCII character.
func Visible(r rune) bool { return AlphaNum(r) || Punct(r) }

// WhiteSpace returns true if ' ', '\t', '\r', or '\n'.
func WhiteSpace(r rune) bool {
	switch r {
	case ' ', '\t', '\r', '\n':
		return true
	default:
		return false
	}
}

// Sign returns true if '+' or '-'
func Sign(r rune) bool { return r == '+' || r == '-' }
