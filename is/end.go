package is

import (
	pegn "gitlab.com/pegn/go"
)

func End(p pegn.Parser) bool {
	return p.Done()
}
