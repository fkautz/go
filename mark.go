// Copyright 2020 Rob Muhlestein.
// Use of this source code is governed by the highly permissive Apache
// 2.0 license that can be found in the LICENSE file.

package pegn

import "fmt"

// Mark is a structure that points to specific position within buffered
// data, akin to a cursor. Marks are returned by methods of Parser
// implementations. Marks must be set to utf.ErrorRune (tk.UNKNOWN) and
// have Len set to 0 if the parser is asked to read beyond the end of the
// data.  Manipulating Mark values directly is discouraged.  Position
// contains the user-land position for reporting
// back when there is a problem with parsing. The Parser.NewLine()
// method should always increment the Line and set ColRune and ColByte
// to 1.
type Mark struct {
	Rune rune     // last rune decoded
	Len  int      // length of last rune decoded (0-4)
	Next int      // offset to beginning of next rune
	Off  struct { // offset position within data
		Rune int
		Byte int
	}
	Pos struct { // user-land representation of position
		Line int // lines (rows) starting at 1
		Rune int // offset rune in line starting at 1
		Byte int // offset byte in line starting at 1
	}
}

// String fulfills the Stringer interface.
func (m *Mark) String() string {
	s := fmt.Sprintf(`%U %q %v,%v-%v (%v-%v)`,
		m.Rune, m.Rune,
		m.Pos.Line, m.Pos.Rune, m.Pos.Byte,
		m.Off.Rune, m.Off.Byte,
	)
	return s
}

// Print prints the mark itself.
func (m *Mark) Print() { fmt.Println(m) }
