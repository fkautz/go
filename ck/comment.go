package ck

import (
	pegn "gitlab.com/pegn/go"
	"gitlab.com/pegn/go/is"
)

// Comment looks for consequetive spaces.
func Comment(p pegn.Parser) *pegn.Mark {
	return p.Check('#', is.Min{is.Seq{is.Not{EOL}, is.Any}, 0})
}
