package ck

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/pegn/go/parser"
)

func TestUnicode(t *testing.T) {
	p := parser.New()
	// ------------
	e := p.Init("U+0020")
	require.Nil(t, e)
	m := p.Check(Unicode)
	require.NotNil(t, m)
	require.Equal(t, 6, m.Off.Byte)
	require.Equal(t, p.Parse(m), "U+0020")
	// ------------
	e = p.Init("U+F0020")
	require.Nil(t, e)
	m = p.Check(Unicode)
	require.NotNil(t, m)
	require.Equal(t, 7, m.Off.Byte)
	require.Equal(t, p.Parse(m), "U+F0020")
	// ------------
	e = p.Init("U+FF0020")
	require.Nil(t, e)
	m = p.Check(Unicode)
	require.NotNil(t, m)
	require.Equal(t, 8, m.Off.Byte)
	require.Equal(t, p.Parse(m), "U+FF0020")
	// ------------
	e = p.Init("U+FFF0020")
	require.Nil(t, e)
	m = p.Check(Unicode)
	require.NotNil(t, m)
	require.Equal(t, 9, m.Off.Byte)
	require.Equal(t, p.Parse(m), "U+FFF0020")
	// ------------
	e = p.Init("U+FFFF0020")
	require.Nil(t, e)
	m = p.Check(Unicode)
	require.NotNil(t, m)
	require.Equal(t, 10, m.Off.Byte)
	require.Equal(t, p.Parse(m), "U+FFFF0020")
	// ------------
	e = p.Init("U+FFFFF0020")
	require.Nil(t, e)
	m = p.Check(Unicode)
	require.Nil(t, m)
}
