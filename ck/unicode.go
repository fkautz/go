package ck

import (
	pegn "gitlab.com/pegn/go"
	"gitlab.com/pegn/go/is"
)

// Unicode looks for a Unicode code point ("U+" upperhex{4,8}).
func Unicode(p pegn.Parser) *pegn.Mark {
	return p.Check("U+", is.MinMax{is.UpperHex, 4, 8})
}
