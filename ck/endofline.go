package ck

import (
	pegn "gitlab.com/pegn/go"
)

func EOL(p pegn.Parser) *pegn.Mark {
	if m := p.Check('\n'); m != nil {
		return m
	}
	if m := p.Check("\r\n"); m != nil {
		return m
	}
	if m := p.Check('\r'); m != nil {
		return m
	}
	return nil
}
