package ck_test

import (
	"fmt"

	"gitlab.com/pegn/go/ck"
	"gitlab.com/pegn/go/parser"
)

func ExampleComment() {
	p := parser.New()
	p.Init("# some thing 👍 nice\n")
	m := p.Check(ck.Comment)
	m.Print()
	fmt.Println(p.Parse(m))
	// Output:
	// U+0065 'e' 1,19-22 (19-22)
	// # some thing 👍 nice
}
