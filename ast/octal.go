package ast

import (
	pegn "gitlab.com/pegn/go"
	"gitlab.com/pegn/go/ast/nd"
	"gitlab.com/pegn/go/is"
)

// Octal <-- "o" octdig+
func Octal(p pegn.Parser) *pegn.Node {

	node := new(pegn.Node)
	node.Type = nd.Octal

	var m *pegn.Mark

	// "o" oct+
	m = p.Check("o", is.Min{is.Octdig, 1})
	if m == nil {
		return nil
	}
	node.Value += p.Parse(m)
	p.Goto(m)
	p.Next()

	return node
}
