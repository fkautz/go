package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExampleSimple() {

	// Simple <- ResClassId / ClassId / ResTokenId / TokenId
	//			 / Literal / Range
	p := parser.New()

	// any
	p.Init("any")
	ast.Simple(p).Print()

	// other
	p.Init("other")
	ast.Simple(p).Print()

	// SP
	p.Init("SP")
	ast.Simple(p).Print()

	// OTHER
	p.Init("OTHER")
	ast.Simple(p).Print()

	// lit
	p.Init("lit")
	ast.Simple(p).Print()

	// [0-9]
	p.Init("[0-9]")
	ast.Simple(p).Print()

	// Output:
	// [46,"any"]
	// [15,"other"]
	// [47,"SP"]
	// [16,"OTHER"]
	// [15,"lit"]
	// [33,[[41,"0"],[41,"9"]]]

}
