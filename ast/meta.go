package ast

import (
	pegn "gitlab.com/pegn/go"
)

// Meta <- "# " Language " (" Version ") " Home EndLine
func Meta(p pegn.Parser) *pegn.Node {

	node := new(pegn.Node)

	var n *pegn.Node

	// "# "
	if p.Expect("# ") == nil {
		return nil
	}

	// Language
	n = Language(p)
	if n == nil {
		return nil
	}
	node.AdoptFrom(n)

	// " ("
	if p.Expect(" (") == nil {
		return nil
	}

	// Version
	n = Version(p)
	if n == nil {
		return nil
	}
	node.AdoptFrom(n)

	// ") "
	if p.Expect(") ") == nil {
		return nil
	}

	// Home
	n = Home(p)
	if n == nil {
		return nil
	}
	node.AppendChild(n)

	// EndLine
	n = EndLine(p)
	if n == nil {
		return nil
	}
	node.AppendChild(n)

	return node
}
