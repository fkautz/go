package ast

import (
	pegn "gitlab.com/pegn/go"
	"gitlab.com/pegn/go/ast/nd"
	"gitlab.com/pegn/go/is"
)

// Binary <-- "b" bitdig+
func Binary(p pegn.Parser) *pegn.Node {

	node := new(pegn.Node)
	node.Type = nd.Binary

	var m *pegn.Mark

	// "b" bin+
	m = p.Check("b", is.Min{is.Bitdig, 1})
	if m == nil {
		return nil
	}
	node.Value += p.Parse(m)
	p.Goto(m)
	p.Next()

	return node
}
