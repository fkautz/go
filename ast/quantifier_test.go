package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExampleQuantifier() {

	// Quantifier  <- Optional / MinZero / MinOne / MinMax / Count
	p := parser.New()

	// ?
	p.Init("?")
	ast.Quantifier(p).Print()

	// +
	p.Init("+")
	ast.Quantifier(p).Print()

	// *
	p.Init("*")
	ast.Quantifier(p).Print()

	// {1,3}
	p.Init("{1,3}")
	ast.Quantifier(p).Print()

	// {5}
	p.Init("{5}")
	ast.Quantifier(p).Print()

	// Output:
	// [24,"?"]
	// [26,"+"]
	// [25,"*"]
	// [27,[[28,"1"],[29,"3"]]]
	// [30,"5"]

}
