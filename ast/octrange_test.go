package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExampleOctRange() {

	// OctRange <-- "[" Octal "-" Octal "]"
	p := parser.New()

	// [o0-o7]
	p.Init("[o0-o7]")
	ast.OctRange(p).Print()

	// [o00-o77]
	p.Init("[o00-o77]")
	ast.OctRange(p).Print()

	// Output:
	// [36,[[44,"o0"],[44,"o7"]]]
	// [36,[[44,"o00"],[44,"o77"]]]

}
