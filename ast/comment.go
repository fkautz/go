package ast

import (
	pegn "gitlab.com/pegn/go"
	"gitlab.com/pegn/go/ast/nd"
	"gitlab.com/pegn/go/is"
)

// Comment <-- (!EndLine any)+
func Comment(p pegn.Parser) *pegn.Node {

	node := new(pegn.Node)
	node.Type = nd.Comment

	var m *pegn.Mark
	var count int

	// (!EndLine any)*
	count = 0
	for {

		b := p.Mark()

		// !EndLine
		if EndLine(p) != nil {
			p.Goto(b)
			break
		}

		// any
		m = p.Check(is.Any)
		if m == nil {
			p.Goto(b)
			break
		}
		node.Value += p.Parse(m)
		p.Next()

		count++
	}

	if !(count >= 1) {
		return nil
	}

	return node
}
