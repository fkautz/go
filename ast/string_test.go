package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExampleString() {

	// String <-- quotable+
	p := parser.New()

	// ! a
	p.Init("! a")
	ast.String(p).Print()

	// !
	p.Init("!")
	ast.String(p).Print()

	// <--
	p.Init("<--")
	ast.String(p).Print()

	// Output:
	// [37,"! a"]
	// [37,"!"]
	// [37,"<--"]

}
