package ast

import (
	pegn "gitlab.com/pegn/go"
)

// LookAhead <- (PosLook / NegLook) Spacing
func LookAhead(p pegn.Parser) *pegn.Node {

	node := new(pegn.Node)
	var n *pegn.Node

	for {

		// PosLook
		n = PosLook(p)
		if n != nil {
			node.AppendChild(n)
			break
		}

		// NegLook
		n = NegLook(p)
		if n != nil {
			node.AppendChild(n)
			break
		}

		return nil

	}

	// Spacing
	n = Spacing(p)
	if n == nil {
		return nil
	}

	return node
}
