package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExampleMinorVer() {

	// MinorVer <-- digit+
	p := parser.New()

	// 31
	p.Init("31")
	ast.MinorVer(p).Print()

	// Output:
	// [12,"31"]

}
