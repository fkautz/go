package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExampleLookAhead() {

	// LookAhead <- (PosLook / NegLook) Spacing
	p := parser.New()

	// !Rule
	p.Init("!Rule ")
	ast.LookAhead(p).Print()

	// &Rule
	p.Init("&Rule ")
	ast.LookAhead(p).Print()

	// &Rule (without a space)
	p.Init("&Rule")
	ast.LookAhead(p).Print()

	// Output:
	// [0,[[21,[[22,[[14,"Rule"]]]]]]]
	// [0,[[20,[[22,[[14,"Rule"]]]]]]]
	// <nil>

}
