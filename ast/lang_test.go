package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExampleLang() {

	// Lang <-- upper{2,12}
	p := parser.New()

	// PEGN
	p.Init("PEGN")
	ast.Lang(p).Print()

	// Output:
	// [9,"PEGN"]

}
