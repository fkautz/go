package ast

import (
	pegn "gitlab.com/pegn/go"
	"gitlab.com/pegn/go/ast/nd"
	"gitlab.com/pegn/go/is"
)

// ClassId <-- lower lower+
func ClassId(p pegn.Parser) *pegn.Node {

	node := new(pegn.Node)
	node.Type = nd.ClassId

	var m *pegn.Mark

	// lower lower+
	m = p.Check(is.Lower, is.Min{is.Lower, 1})
	if m == nil {
		return nil
	}
	node.Value = p.Parse(m)
	p.Goto(m)
	p.Next()

	return node
}
