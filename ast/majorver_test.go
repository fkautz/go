package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExampleMajorVer() {

	// MajorVer <-- digit+
	p := parser.New()

	// 0
	p.Init("0")
	ast.MajorVer(p).Print()

	// Output:
	// [11,"0"]

}
