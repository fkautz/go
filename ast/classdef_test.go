package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExampleClassDef() {

	// ClassDef <-- (ResClassId / ClassId) SP+ "<-"  SP+ ClassExpr
	p := parser.New()

	// alpha <- [A-Z] / [a-z]
	p.Init("alpha <- [A-Z] / [a-z]")
	ast.ClassDef(p).Print()

	// alnum <- alphanum
	p.Init("alnum <- alphanum")
	ast.ClassDef(p).Print()

	// Output:
	// [7,[[46,"alpha"],[18,[[32,[[38,"A"],[38,"Z"]]],[32,[[38,"a"],[38,"z"]]]]]]]
	// [7,[[46,"alnum"],[18,[[46,"alphanum"]]]]]

}
