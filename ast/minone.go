package ast

import (
	pegn "gitlab.com/pegn/go"
	"gitlab.com/pegn/go/ast/nd"
)

// MinOne <-- "+"
func MinOne(p pegn.Parser) *pegn.Node {

	node := new(pegn.Node)
	node.Type = nd.MinOne
	node.Value = "+"

	if p.Expect("+") == nil {
		return nil
	}

	return node
}
