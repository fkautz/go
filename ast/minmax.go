package ast

import (
	pegn "gitlab.com/pegn/go"
	"gitlab.com/pegn/go/ast/nd"
)

// MinMax <-- "{" Min "," Max? "}"
func MinMax(p pegn.Parser) *pegn.Node {

	node := new(pegn.Node)
	node.Type = nd.MinMax

	var n *pegn.Node
	beg := p.Mark()

	// "{"
	if p.Expect("{") == nil {
		return nil
	}

	// Min
	n = Min(p)
	if n == nil {
		p.Goto(beg)
		return nil
	}
	node.AppendChild(n)

	// ","
	if p.Expect(",") == nil {
		p.Goto(beg)
		return nil
	}

	// Max
	n = Max(p)
	if n != nil {
		node.AppendChild(n)
	}

	// "}"
	if p.Expect("}") == nil {
		p.Goto(beg)
		return nil
	}

	return node
}
