package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExampleIntRange() {

	// IntRange <-- "[" Integer "-" Integer "]"
	p := parser.New()

	// [0-9]
	p.Init("[0-9]")
	ast.IntRange(p).Print()

	// [00-99]
	p.Init("[00-99]")
	ast.IntRange(p).Print()

	// Output:
	// [33,[[41,"0"],[41,"9"]]]
	// [33,[[41,"00"],[41,"99"]]]

}
