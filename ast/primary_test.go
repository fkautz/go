package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExamplePrimary() {

	// Primary <-- Simple / CheckId / '(' Expression ')'
	p := parser.New()

	// TK
	p.Init("TK")
	ast.Primary(p).Print()

	// CheckId
	p.Init("CheckId")
	ast.Primary(p).Print()

	// "<--"
	p.Init("\"<--\"")
	ast.Primary(p).Print()

	// (Expression)
	p.Init("(Expression)")
	ast.Primary(p).Print()

	// Output:
	// [16,"TK"]
	// [14,"CheckId"]
	// [37,"<--"]
	// [17,[[19,[[22,[[14,"Expression"]]]]]]]

}
