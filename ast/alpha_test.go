package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExampleAlpha() {

	// Alpha <-- alpha
	p := parser.New()

	// a
	p.Init("a")
	ast.Alpha(p).Print()

	// A
	p.Init("A")
	ast.Alpha(p).Print()

	// z
	p.Init("z")
	ast.Alpha(p).Print()

	// Z
	p.Init("Z")
	ast.Alpha(p).Print()

	// _
	p.Init("_")
	ast.Alpha(p).Print()

	// Output:
	// [38,"a"]
	// [38,"A"]
	// [38,"z"]
	// [38,"Z"]
	// <nil>

}
