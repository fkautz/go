package ast

import (
	pegn "gitlab.com/pegn/go"
	"gitlab.com/pegn/go/ast/nd"
)

// UniRange <-- "[" Uni "-" Uni "]"
func UniRange(p pegn.Parser) *pegn.Node {

	node := new(pegn.Node)
	node.Type = nd.UniRange

	var n *pegn.Node
	beg := p.Mark()

	// "["
	if p.Expect("[") == nil {
		p.Goto(beg)
		return nil
	}

	// Uni
	n = Unicode(p)
	if n == nil {
		p.Goto(beg)
		return nil
	}
	node.AppendChild(n)

	// "-"
	if p.Expect("-") == nil {
		p.Goto(beg)
		return nil
	}

	// Uni
	n = Unicode(p)
	if n == nil {
		p.Goto(beg)
		return nil
	}
	node.AppendChild(n)

	// "]"
	if p.Expect("]") == nil {
		p.Goto(beg)
		return nil
	}

	return node
}
