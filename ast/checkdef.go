package ast

import (
	pegn "gitlab.com/pegn/go"
	"gitlab.com/pegn/go/ast/nd"
	"gitlab.com/pegn/go/is"
)

// CheckDef <-- CheckId SP+ "<-" SP+ Expression
func CheckDef(p pegn.Parser) *pegn.Node {

	node := new(pegn.Node)
	node.Type = nd.CheckDef

	var n *pegn.Node
	beg := p.Mark()

	// CheckId
	n = CheckId(p)
	if n == nil {
		p.Goto(beg)
		return nil
	}
	node.AppendChild(n)

	// SP+ "<-" SP+
	if p.Expect(is.Min{' ', 1}, "<-", is.Min{' ', 1}) == nil {
		p.Goto(beg)
		return nil
	}

	// Expression
	n = Expression(p)
	if n == nil {
		p.Goto(beg)
		return nil
	}
	node.AppendChild(n)

	return node
}
