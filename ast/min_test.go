package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExampleMin() {

	// Min <-- digit+
	p := parser.New()

	// 1
	p.Init("1")
	ast.Min(p).Print()

	// 99
	p.Init("99P")
	ast.Min(p).Print()
	p.Print()

	// Output:
	// [28,"1"]
	// [28,"99"]
	// U+0050 'P' 1,3-3 (3-3)

}
