package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExampleInteger() {

	p := parser.New()

	// 0
	p.Init("0")
	ast.Integer(p).Print()

	// 99
	p.Init("99")
	ast.Integer(p).Print()

	// Output:
	// [41,"0"]
	// [41,"99"]

}
