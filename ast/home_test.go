package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExampleHome() {

	// Home <-- (!ws any)+
	p := parser.New()

	// gitlab.com/pegn/spec
	p.Init("gitlab.com/pegn/spec")
	ast.Home(p).Print()

	// Output:
	// [3,"gitlab.com/pegn/spec"]

}
