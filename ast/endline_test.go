package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExampleEndLine() {

	// EndLine <-- LF / CR LF / CR
	p := parser.New()

	// LF
	p.Init("\n")
	ast.EndLine(p).Print()

	// CR LF
	p.Init("\r\n")
	ast.EndLine(p).Print()

	// CR
	p.Init("\r")
	ast.EndLine(p).Print()

	// Output:
	// [45,"\n"]
	// [45,"\r\n"]
	// [45,"\r"]

}
