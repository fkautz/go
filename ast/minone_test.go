package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExampleMinOne() {

	// MinOne <-- "+"
	p := parser.New()

	// +
	p.Init("+")
	ast.MinOne(p).Print()

	// Output:
	// [26,"+"]

}
