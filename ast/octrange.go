package ast

import (
	pegn "gitlab.com/pegn/go"
	"gitlab.com/pegn/go/ast/nd"
)

// OctRange <-- "[" Octal "-" Octal "]"
func OctRange(p pegn.Parser) *pegn.Node {

	node := new(pegn.Node)
	node.Type = nd.OctRange

	var n *pegn.Node
	beg := p.Mark()

	// "["
	if p.Expect("[") == nil {
		p.Goto(beg)
		return nil
	}

	// Octal
	n = Octal(p)
	if n == nil {
		p.Goto(beg)
		return nil
	}
	node.AppendChild(n)

	// "-"
	if p.Expect("-") == nil {
		p.Goto(beg)
		return nil
	}

	// Octal
	n = Octal(p)
	if n == nil {
		p.Goto(beg)
		return nil
	}
	node.AppendChild(n)

	// "]"
	if p.Expect("]") == nil {
		p.Goto(beg)
		return nil
	}

	return node
}
