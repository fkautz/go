package ast

import (
	pegn "gitlab.com/pegn/go"
	"gitlab.com/pegn/go/ast/nd"
	"gitlab.com/pegn/go/is"
)

// ClassDef <-- (ResClassId / ClassId) SP+ "<-"  SP+ ClassExpr
func ClassDef(p pegn.Parser) *pegn.Node {

	node := new(pegn.Node)
	node.Type = nd.ClassDef

	var n *pegn.Node
	beg := p.Mark()

	// (ResClassId / ClassId)
	for {

		// ResClassId
		n = ResClassId(p)
		if n != nil {
			break
		}

		// ClassId
		n = ClassId(p)
		if n != nil {
			break
		}

		p.Goto(beg)
		return nil
	}
	node.AppendChild(n)

	// SP+ "<-" SP+
	if p.Expect(is.Min{' ', 1}, "<-", is.Min{' ', 1}) == nil {
		p.Goto(beg)
		return nil
	}

	// ClassExpr
	n = ClassExpr(p)
	if n == nil {
		p.Goto(beg)
		return nil
	}
	node.AppendChild(n)

	return node
}
