package ast

import (
	pegn "gitlab.com/pegn/go"
	"gitlab.com/pegn/go/ast/nd"
	"gitlab.com/pegn/go/is"
)

// String <-- quotable+
func String(p pegn.Parser) *pegn.Node {

	node := new(pegn.Node)
	node.Type = nd.String

	var m *pegn.Mark

	// quotable+
	m = p.Check(is.Min{is.Quotable, 1})
	if m == nil {
		return nil
	}
	node.Value += p.Parse(m)
	p.Goto(m)
	p.Next()

	return node
}
