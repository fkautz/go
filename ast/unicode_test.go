package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExampleUnicode() {

	// Unicode <-- "U+" upperhex{4,8}
	p := parser.New()

	// U+0000
	p.Init("U+0000")
	ast.Unicode(p).Print()

	// U+ffffff
	p.Init("U+ffffff")
	ast.Unicode(p).Print()

	// U+FFFFFF
	p.Init("U+FFFFFF")
	ast.Unicode(p).Print()

	// Output:
	// [40,"U+0000"]
	// <nil>
	// [40,"U+FFFFFF"]

}
