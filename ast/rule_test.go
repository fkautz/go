package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExampleRule() {

	// Rule <-- Primary Quantifier?
	p := parser.New()

	// "<--"
	p.Init("\"<--\"")
	ast.Rule(p).Print()

	// CheckId
	p.Init("CheckId")
	ast.Rule(p).Print()

	// CheckId{2}
	p.Init("CheckId{2}")
	ast.Rule(p).Print()

	// Rule Another
	p.Init("Rule Another")
	ast.Rule(p).Print()
	ast.Spacing(p)
	ast.Rule(p).Print()

	// TODO: fix "\u003c--", should be "<--"

	// Output:
	// [22,[[37,"\u003c--"]]]
	// [22,[[14,"CheckId"]]]
	// [22,[[14,"CheckId"],[30,"2"]]]
	// [22,[[14,"Rule"]]]
	// [22,[[14,"Another"]]]

}
