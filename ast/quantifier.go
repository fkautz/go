package ast

import (
	pegn "gitlab.com/pegn/go"
)

// Quantifier <- Optional / MinZero / MinOne / MinMax / Count
func Quantifier(p pegn.Parser) *pegn.Node {

	var node *pegn.Node

	// Optional
	node = Optional(p)
	if node != nil {
		return node
	}

	// MinZero
	node = MinZero(p)
	if node != nil {
		return node
	}

	// MinOne
	node = MinOne(p)
	if node != nil {
		return node
	}

	// MinMax
	node = MinMax(p)
	if node != nil {
		return node
	}

	// Count
	node = Count(p)
	if node != nil {
		return node
	}

	return nil
}
