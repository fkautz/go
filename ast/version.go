package ast

import (
	pegn "gitlab.com/pegn/go"
)

// Version <- 'v' MajorVer '.' MinorVer '.' PatchVer
func Version(p pegn.Parser) *pegn.Node {

	node := new(pegn.Node)
	var n *pegn.Node

	// 'v'
	if p.Expect('v') == nil {
		return nil
	}

	// MajorVer
	n = MajorVer(p)
	if n == nil {
		return nil
	}
	node.AppendChild(n)

	// '.'
	if p.Expect('.') == nil {
		return nil
	}

	// MinorVer
	n = MinorVer(p)
	if n == nil {
		return nil
	}
	node.AppendChild(n)

	// '.'
	if p.Expect('.') == nil {
		return nil
	}

	// PatchVer
	n = PatchVer(p)
	if n == nil {
		return nil
	}
	node.AppendChild(n)

	return node
}
