package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExampleBinary() {

	// Binary <-- "b" bitdig+
	p := parser.New()

	// b01
	p.Init("b01")
	ast.Binary(p).Print()

	// b01010110
	p.Init("b01010110")
	ast.Binary(p).Print()

	// Output:
	// [42,"b01"]
	// [42,"b01010110"]

}
