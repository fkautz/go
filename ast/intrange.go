package ast

import (
	pegn "gitlab.com/pegn/go"
	"gitlab.com/pegn/go/ast/nd"
)

// IntRange <-- "[" Integer "-" Integer "]"
func IntRange(p pegn.Parser) *pegn.Node {

	node := new(pegn.Node)
	node.Type = nd.IntRange

	var n *pegn.Node
	beg := p.Mark()

	// "["
	if p.Expect("[") == nil {
		p.Goto(beg)
		return nil
	}

	// Integer
	n = Integer(p)
	if n == nil {
		p.Goto(beg)
		return nil
	}
	node.AppendChild(n)

	// "-"
	if p.Expect("-") == nil {
		p.Goto(beg)
		return nil
	}

	// Integer
	n = Integer(p)
	if n == nil {
		p.Goto(beg)
		return nil
	}
	node.AppendChild(n)

	// "]"
	if p.Expect("]") == nil {
		p.Goto(beg)
		return nil
	}

	return node
}
