package ast

import (
	pegn "gitlab.com/pegn/go"
)

// Simple <- ResClassId / ClassId / ResTokenId / TokenId
//         / Literal / Range
func Simple(p pegn.Parser) *pegn.Node {

	var node *pegn.Node

	// ResClassId
	node = ResClassId(p)
	if node != nil {
		return node
	}

	// ClassId
	node = ClassId(p)
	if node != nil {
		return node
	}

	// ResTokenId
	node = ResTokenId(p)
	if node != nil {
		return node
	}

	// TokenId
	node = TokenId(p)
	if node != nil {
		return node
	}

	// Literal
	node = Literal(p)
	if node != nil {
		return node
	}

	// Range
	node = Range(p)
	if node != nil {
		return node
	}

	return nil
}
