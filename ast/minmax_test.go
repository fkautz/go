package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExampleMinMax() {

	// MinMax <-- "{" Min "," Max? "}"
	p := parser.New()

	// {1,}
	p.Init("{1,}")
	ast.MinMax(p).Print()

	// {0,9}
	p.Init("{0,9}")
	ast.MinMax(p).Print()

	// Output:
	// [27,[[28,"1"]]]
	// [27,[[28,"0"],[29,"9"]]]

}
