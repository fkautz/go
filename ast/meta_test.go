package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExampleMeta() {

	// Meta <- "# " Language " (" Version ") " Home EndLine
	p := parser.New()

	// # PEGN (v0.31.1) gitlab.com/pegn/spec
	p.Init("# PEGN (v0.31.1) gitlab.com/pegn/spec\n")
	ast.Meta(p).Print()

	// Output:
	// [0,[[9,"PEGN"],[11,"0"],[12,"31"],[13,"1"],[3,"gitlab.com/pegn/spec"],[45,"\n"]]]

}
