package ast

import (
	pegn "gitlab.com/pegn/go"
	"gitlab.com/pegn/go/ast/nd"
)

// MinZero <-- "*"
func MinZero(p pegn.Parser) *pegn.Node {

	node := new(pegn.Node)
	node.Type = nd.MinZero
	node.Value = "*"

	if p.Expect("*") == nil {
		return nil
	}

	return node
}
