package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExampleComEndLine() {

	// ComEndLine <- SP* ("# " Comment)? EndLine
	p := parser.New()

	// SP\n
	p.Init(" \n")
	ast.ComEndLine(p).Print()

	// # C\n
	p.Init("# C\n")
	ast.ComEndLine(p).Print()

	// Output:
	// [0,[[45,"\n"]]]
	// [0,[[4,"# C"],[45,"\n"]]]

}
