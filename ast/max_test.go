package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExampleMax() {

	// Max <-- digit+
	p := parser.New()

	// 1
	p.Init("1")
	ast.Max(p).Print()

	// 99
	p.Init("99")
	ast.Max(p).Print()

	// Output:
	// [29,"1"]
	// [29,"99"]

}
