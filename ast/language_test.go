package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExampleLanguage() {

	// Language <- Lang ("-" LangExt)?
	p := parser.New()

	// PEGN
	p.Init("PEGN")
	ast.Language(p).Print()

	// PEGN-alpa
	p.Init("PEGN-alpha")
	ast.Language(p).Print()

	// Output:
	// [0,[[9,"PEGN"]]]
	// [0,[[9,"PEGN"],[10,"alpha"]]]

}
