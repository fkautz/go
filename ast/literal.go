package ast

import (
	pegn "gitlab.com/pegn/go"
)

// Literal <- DQ String DQ / Unicode / Binary / Hexadec / Octal
func Literal(p pegn.Parser) *pegn.Node {

	var n *pegn.Node

	// DQ String DQ
	for {

		b := p.Mark()

		// DQ
		if p.Expect('"') == nil {
			p.Goto(b)
			break
		}

		// String
		n = String(p)
		if n == nil {
			p.Goto(b)
			break
		}

		// DQ
		if p.Expect('"') == nil {
			p.Goto(b)
			break
		}

		return n
	}

	// Unicode
	n = Unicode(p)
	if n != nil {
		return n
	}

	// Binary
	n = Binary(p)
	if n != nil {
		return n
	}

	// Hexadec
	n = Hexadec(p)
	if n != nil {
		return n
	}

	// Octal
	n = Octal(p)
	if n != nil {
		return n
	}

	return nil
}
