package ast

import (
	pegn "gitlab.com/pegn/go"
	"gitlab.com/pegn/go/ast/nd"
	"gitlab.com/pegn/go/is"
)

// CheckId <- (upper lower+)+
func CheckId(p pegn.Parser) *pegn.Node {

	node := new(pegn.Node)
	node.Type = nd.CheckId

	var m *pegn.Mark

	// (upper lower+)+
	m = p.Check(is.Min{is.Seq{is.Upper, is.Min{is.Lower, 1}}, 1})
	if m == nil {
		return nil
	}
	node.Value = p.Parse(m)
	p.Goto(m)
	p.Next()

	return node
}
