package ast

import (
	pegn "gitlab.com/pegn/go"
	"gitlab.com/pegn/go/ast/nd"
	"gitlab.com/pegn/go/is"
)

// LangExt <-- visible{1,20}
func LangExt(p pegn.Parser) *pegn.Node {

	node := new(pegn.Node)
	node.Type = nd.LangExt

	var m *pegn.Mark

	// visiblt{1,20}
	m = p.Check(is.MinMax{is.Visible, 1, 20})
	if m == nil {
		return nil
	}
	node.Value += p.Parse(m)
	p.Goto(m)
	p.Next()

	return node
}
