package ast

import (
	pegn "gitlab.com/pegn/go"
	"gitlab.com/pegn/go/ast/nd"
	"gitlab.com/pegn/go/is"
)

// Alpha <-- alpha
func Alpha(p pegn.Parser) *pegn.Node {

	node := new(pegn.Node)
	node.Type = nd.Alpha

	var m *pegn.Mark

	// alpha
	m = p.Check(is.Alpha)
	if m == nil {
		return nil
	}
	node.Value += p.Parse(m)
	p.Goto(m)
	p.Next()

	return node
}
