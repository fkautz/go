package ast

import (
	pegn "gitlab.com/pegn/go"
	"gitlab.com/pegn/go/ast/nd"
)

// Rule <-- Primary Quantifier?
func Rule(p pegn.Parser) *pegn.Node {

	node := new(pegn.Node)
	node.Type = nd.Rule

	var n *pegn.Node

	// Primary
	n = Primary(p)
	if n == nil {
		return nil
	}
	node.AppendChild(n)

	// Quantifier?
	n = Quantifier(p)
	if n != nil {
		node.AppendChild(n)
	}

	return node
}
