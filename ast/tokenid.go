package ast

import (
	pegn "gitlab.com/pegn/go"
	"gitlab.com/pegn/go/ast/nd"
	"gitlab.com/pegn/go/is"
)

// TokenId <-- upper upper+
func TokenId(p pegn.Parser) *pegn.Node {

	node := new(pegn.Node)
	node.Type = nd.TokenId

	var m *pegn.Mark

	// upper upper+
	m = p.Check(is.Upper, is.Min{is.Upper, 1})
	if m == nil {
		return nil
	}
	node.Value = p.Parse(m)
	p.Goto(m)
	p.Next()

	return node
}
