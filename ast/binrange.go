package ast

import (
	pegn "gitlab.com/pegn/go"
	"gitlab.com/pegn/go/ast/nd"
)

// BinRange <-- "[" Bin "-" Bin "]"
func BinRange(p pegn.Parser) *pegn.Node {

	node := new(pegn.Node)
	node.Type = nd.BinRange

	var n *pegn.Node
	beg := p.Mark()

	// "["
	if p.Expect("[") == nil {
		p.Goto(beg)
		return nil
	}

	// Binary
	n = Binary(p)
	if n == nil {
		p.Goto(beg)
		return nil
	}
	node.AppendChild(n)

	// "-"
	if p.Expect("-") == nil {
		p.Goto(beg)
		return nil
	}

	// Binary
	n = Binary(p)
	if n == nil {
		p.Goto(beg)
		return nil
	}
	node.AppendChild(n)

	// "]"
	if p.Expect("]") == nil {
		p.Goto(beg)
		return nil
	}

	return node
}
