package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExampleRange() {

	// Range <- AlphaRange / IntRange / UniRange
	//        / BinRange / HexRange / OctRange
	p := parser.New()

	// [U+0000-U+00FF]
	p.Init("[U+0000-U+00FF]")
	ast.UniRange(p).Print()

	// [a-z]
	p.Init("[a-z]")
	ast.AlphaRange(p).Print()

	// [00-99]
	p.Init("[00-99]")
	ast.IntRange(p).Print()

	// [b01-b10]
	p.Init("[b01-b10]")
	ast.BinRange(p).Print()

	// [x0-xF]
	p.Init("[x0-xF]")
	ast.HexRange(p).Print()

	// Output:
	// [31,[[40,"U+0000"],[40,"U+00FF"]]]
	// [32,[[38,"a"],[38,"z"]]]
	// [33,[[41,"00"],[41,"99"]]]
	// [34,[[42,"b01"],[42,"b10"]]]
	// [35,[[43,"x0"],[43,"xF"]]]

}
