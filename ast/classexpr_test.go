package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExampleClassExpr() {

	// ClassExpr <-- Simple (Spacing "/" SP+ Simple)*
	p := parser.New()

	// [A-Z] / [a-z]
	p.Init("[A-Z] / [a-z]")
	ast.ClassExpr(p).Print()

	// alphanum / punct
	p.Init("alphanum / punct")
	ast.ClassExpr(p).Print()

	// SP / TAB / CR / LF
	p.Init("SP / TAB / CR / LF")
	ast.ClassExpr(p).Print()

	// Output:
	// [18,[[32,[[38,"A"],[38,"Z"]]],[32,[[38,"a"],[38,"z"]]]]]
	// [18,[[46,"alphanum"],[46,"punct"]]]
	// [18,[[47,"SP"],[47,"TAB"],[47,"CR"],[47,"LF"]]]

}
