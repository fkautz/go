package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExampleLangExt() {

	// LangExt <-- visible{1,20}
	p := parser.New()

	// alpha
	p.Init("alpha")
	ast.LangExt(p).Print()

	// Output:
	// [10,"alpha"]

}
