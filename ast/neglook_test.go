package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExampleNegLook() {

	// NegLook <-- '!' Rule
	p := parser.New()

	// !Rule
	p.Init("!Rule")
	ast.NegLook(p).Print()

	// Output:
	// [21,[[22,[[14,"Rule"]]]]]

}
