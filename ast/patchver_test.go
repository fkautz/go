package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExamplePatchVer() {

	// PatchVer <-- digit+
	p := parser.New()

	// 1
	p.Init("1")
	ast.PatchVer(p).Print()

	// Output:
	// [13,"1"]

}
