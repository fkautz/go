package ast

import (
	pegn "gitlab.com/pegn/go"
	"gitlab.com/pegn/go/ast/nd"
)

// Sequence <-- LookAhead* Rule (Spacing Rule)*
func Sequence(p pegn.Parser) *pegn.Node {

	node := new(pegn.Node)
	node.Type = nd.Sequence

	var n *pegn.Node

	// LookAhead*
	for {
		n = LookAhead(p)
		if n == nil {
			break
		}
		node.AdoptFrom(n)
	}

	// Rule
	n = Rule(p)
	if n == nil {
		return nil
	}
	node.AppendChild(n)

	// (Spacing Rule)*
	for {

		beg := p.Mark()

		// Spacing
		sp := Spacing(p)
		if sp == nil {
			p.Goto(beg)
			break
		}

		// Rule
		ru := Rule(p)
		if ru == nil {
			p.Goto(beg)
			break
		}

		// This is an edge case this can be a significant node
		// (ComEndLine) but this is optional.
		// The node can also be empty.
		if len(sp.Children()) != 0 {
			node.AppendChild(sp)
		}
		node.AppendChild(ru)
	}

	return node
}
