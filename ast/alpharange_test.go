package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExampleAlphaRange() {

	// AlphaRange <-- "[" Alpha "-" Alpha "]"
	p := parser.New()

	// [a-z]
	p.Init("[a-z]")
	ast.AlphaRange(p).Print()

	// [A-Z]
	p.Init("[A-Z]")
	ast.AlphaRange(p).Print()

	// Output:
	// [32,[[38,"a"],[38,"z"]]]
	// [32,[[38,"A"],[38,"Z"]]]

}
