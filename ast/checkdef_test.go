package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExampleCheckDef() {

	// CheckDef <-- CheckId SP+ "<-" SP+ Expression
	p := parser.New()

	// Rule <- Another
	p.Init("Rule <- Another")
	ast.CheckDef(p).Print()

	// Simple <- ResClassId / ClassId / Etc
	p.Init("Simple <- ResClassId / ClassId / Etc")
	ast.CheckDef(p).Print()

	// Output:
	// [6,[[14,"Rule"],[17,[[19,[[22,[[14,"Another"]]]]]]]]]
	// [6,[[14,"Simple"],[17,[[19,[[22,[[14,"ResClassId"]]]]],[19,[[22,[[14,"ClassId"]]]]],[19,[[22,[[14,"Etc"]]]]]]]]]

}
