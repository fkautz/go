package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExampleBinRange() {

	// BinRange <-- "[" Bin "-" Bin "]"
	p := parser.New()

	// [b01-b10]
	p.Init("[b01-b10]")
	ast.BinRange(p).Print()

	// [b0001-b1111]
	p.Init("[b0001-b1111]")
	ast.BinRange(p).Print()

	// Output:
	// [34,[[42,"b01"],[42,"b10"]]]
	// [34,[[42,"b0001"],[42,"b1111"]]]

}
