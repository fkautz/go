package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExampleCheckId() {

	// CheckId <- (upper lower+)+
	p := parser.New()

	// CheckId
	// AnotherId
	p.Init("CheckId AnotherId")
	ast.CheckId(p).Print()
	ast.Spacing(p)
	ast.CheckId(p).Print()

	// invalid
	p.Init("invalid")
	ast.CheckId(p).Print()

	// Output:
	// [14,"CheckId"]
	// [14,"AnotherId"]
	// <nil>

}
