package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExampleSpacing() {

	// Spacing <- ComEndLine? SP+
	p := parser.New()

	// SP
	p.Init(" ")
	ast.Spacing(p).Print()

	// # Comment
	p.Init("# Comment\n ")
	ast.Spacing(p).Print()

	// Output:
	// []
	// [0,[[4,"# Comment"],[45,"\n"]]]

}
