package ast

import (
	pegn "gitlab.com/pegn/go"
	"gitlab.com/pegn/go/ast/nd"
)

// HexRange <-- "[" Hexadec "-" Hexadec "]"
func HexRange(p pegn.Parser) *pegn.Node {

	node := new(pegn.Node)
	node.Type = nd.HexRange

	var n *pegn.Node
	beg := p.Mark()

	// "["
	if p.Expect("[") == nil {
		p.Goto(beg)
		return nil
	}

	// Hexadec
	n = Hexadec(p)
	if n == nil {
		p.Goto(beg)
		return nil
	}
	node.AppendChild(n)

	// "-"
	if p.Expect("-") == nil {
		p.Goto(beg)
		return nil
	}

	// Hexadec
	n = Hexadec(p)
	if n == nil {
		p.Goto(beg)
		return nil
	}
	node.AppendChild(n)

	// "]"
	if p.Expect("]") == nil {
		p.Goto(beg)
		return nil
	}

	return node
}
