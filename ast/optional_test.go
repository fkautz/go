package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExampleOptional() {

	// Optional <-- "?"
	p := parser.New()

	// ?
	p.Init("?")
	ast.Optional(p).Print()

	// Output:
	// [24,"?"]

}
