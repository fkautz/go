package ast

import (
	pegn "gitlab.com/pegn/go"
	"gitlab.com/pegn/go/ast/nd"
)

// Optional <-- "?"
func Optional(p pegn.Parser) *pegn.Node {

	node := new(pegn.Node)
	node.Type = nd.Optional
	node.Value = "?"

	if p.Expect("?") == nil {
		return nil
	}

	return node
}
