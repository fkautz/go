package ast

import (
	pegn "gitlab.com/pegn/go"
	"gitlab.com/pegn/go/ast/nd"
)

// NegLook <-- '!' Rule
func NegLook(p pegn.Parser) *pegn.Node {

	node := new(pegn.Node)
	node.Type = nd.NegLook

	var n *pegn.Node
	beg := p.Mark()

	// '!'
	if p.Expect('!') == nil {
		p.Goto(beg)
		return nil
	}

	// Rule
	n = Rule(p)
	if n == nil {
		p.Goto(beg)
		return nil
	}
	node.AppendChild(n)

	return node
}
