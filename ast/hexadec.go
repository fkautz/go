package ast

import (
	pegn "gitlab.com/pegn/go"
	"gitlab.com/pegn/go/ast/nd"
	"gitlab.com/pegn/go/is"
)

// Hexadec <-- "x" upperhex+
func Hexadec(p pegn.Parser) *pegn.Node {

	node := new(pegn.Node)
	node.Type = nd.Hexadec

	var m *pegn.Mark

	// "x" upperhex+
	m = p.Check("x", is.Min{is.UpperHex, 1})
	if m == nil {
		return nil
	}
	node.Value += p.Parse(m)
	p.Goto(m)
	p.Next()

	return node
}
