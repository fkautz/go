package ast

import (
	pegn "gitlab.com/pegn/go"
	"gitlab.com/pegn/go/ast/nd"
	"gitlab.com/pegn/go/is"
)

// Count <-- "{" digit+ "}"
func Count(p pegn.Parser) *pegn.Node {

	node := new(pegn.Node)
	node.Type = nd.Count

	var m *pegn.Mark

	// "{"
	if p.Expect("{") == nil {
		return nil
	}

	// digit+
	m = p.Check(is.Min{is.Digit, 1})
	if m == nil {
		return nil
	}
	node.Value += p.Parse(m)
	p.Goto(m)
	p.Next()

	// "}"
	if p.Expect("}") == nil {
		return nil
	}

	return node
}
