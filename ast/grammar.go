package ast

import (
	pegn "gitlab.com/pegn/go"
	"gitlab.com/pegn/go/ast/nd"
)

// Grammar <-- Meta ComEndLine* (Definition ComEndLine*)+ Done
func Grammar(p pegn.Parser) *pegn.Node {

	node := new(pegn.Node)
	node.Type = nd.Grammar

	var count int
	var n *pegn.Node

	// Meta
	n = Meta(p)
	if n == nil {
		return nil
	}
	node.AppendChild(n)

	// ComEndLine*
	for {
		n = ComEndLine(p)
		if n == nil {
			break
		}
		node.AdoptFrom(n)
	}

	// (Definition ComEndLine*)+
	count = 0
	for {

		// Definition
		n = Definition(p)
		if n == nil {
			break
		}
		node.AdoptFrom(n)

		// ComEndLine*
		for {
			n = ComEndLine(p)
			if n == nil {
				break
			}
			node.AdoptFrom(n)
		}

		count++
	}

	if count == 0 {
		return nil
	}

	// Done
	if !p.Done() {
		return nil
	}

	return node
}
