package ast

import (
	pegn "gitlab.com/pegn/go"
	"gitlab.com/pegn/go/ast/nd"
	"gitlab.com/pegn/go/is"
)

// Lang <-- upper{2,12}
func Lang(p pegn.Parser) *pegn.Node {

	node := new(pegn.Node)
	node.Type = nd.Lang

	var m *pegn.Mark

	// upper{2,12}
	m = p.Check(is.MinMax{is.Upper, 2, 12})
	if m == nil {
		return nil
	}
	node.Value += p.Parse(m)
	p.Goto(m)
	p.Next()

	return node
}
