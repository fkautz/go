package ast

import (
	pegn "gitlab.com/pegn/go"
)

// Range <- AlphaRange / IntRange / UniRange
//        / BinRange / HexRange / OctRange
func Range(p pegn.Parser) *pegn.Node {

	node := new(pegn.Node)

	// AlphaRange
	node = AlphaRange(p)
	if node != nil {
		return node
	}

	// IntRange
	node = IntRange(p)
	if node != nil {
		return node
	}

	// UniRange
	node = UniRange(p)
	if node != nil {
		return node
	}

	// BinRange
	node = BinRange(p)
	if node != nil {
		return node
	}

	// HexRange
	node = HexRange(p)
	if node != nil {
		return node
	}

	// OctRange
	node = OctRange(p)
	if node != nil {
		return node
	}

	return nil
}
