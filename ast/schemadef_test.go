package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExampleSchemaDef() {

	// SchemaDef <-- CheckId SP+ "<--" SP+ Expression
	p := parser.New()

	// Schema <-- " "
	p.Init("Schema <-- \" \"")
	ast.SchemaDef(p).Print()

	// Schema <-- SP
	p.Init("Schema <-- SP")
	ast.SchemaDef(p).Print()

	// Output:
	// [5,[[14,"Schema"],[17,[[19,[[22,[[37," "]]]]]]]]]
	// [5,[[14,"Schema"],[17,[[19,[[22,[[47,"SP"]]]]]]]]]

}
