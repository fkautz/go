package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExampleSequence() {

	// Sequence <-- LookAhead* Rule (Spacing Rule)*
	p := parser.New()

	// Rule Another
	p.Init("Rule Another")
	ast.Sequence(p).Print()

	// !Rule Another
	p.Init("!Rule Another")
	ast.Sequence(p).Print()

	// CheckId SP+ "<--" SP+ Expression
	p.Init("CheckId SP+ \"<--\" SP+ Expression")
	ast.Sequence(p).Print()

	// TODO: fix "\u003c--", should be "<--"

	// Output:
	// [19,[[22,[[14,"Rule"]]],[22,[[14,"Another"]]]]]
	// [19,[[21,[[22,[[14,"Rule"]]]]],[22,[[14,"Another"]]]]]
	// [19,[[22,[[14,"CheckId"]]],[22,[[47,"SP"],[26,"+"]]],[22,[[37,"\u003c--"]]],[22,[[47,"SP"],[26,"+"]]],[22,[[14,"Expression"]]]]]

}
