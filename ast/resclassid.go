package ast

import (
	pegn "gitlab.com/pegn/go"
	"gitlab.com/pegn/go/ast/nd"
)

// ResClassId <-- "any" / "upperhex" / "lowerhex" / "alnum" / "alphanum"
//              / "digit" / "punct" / "alpha" / "hexdig" / "octdig"
//              / "bitdig" / "lower" / "upper" / "visible" / "quotable"
//              / "ws" / "sign" / "control" / "ascii" / "blank" / "cntrl"
//              / "graph" / "print" / "space" / "word" / "xdigit"
func ResClassId(p pegn.Parser) *pegn.Node {

	node := new(pegn.Node)
	node.Type = nd.ResClassId

	var m *pegn.Mark

	for _, v := range []string{
		"any", "upperhex", "lowerhex", "alnum", "alphanum",
		"digit", "punct", "alpha", "hexdig", "octdig", "bitdig",
		"lower", "upper", "visible", "quotable", "ws", "sign",
		"control", "ascii", "blank", "cntrl", "graph", "print",
		"space", "word", "xdigit",
	} {
		m = p.Check(v)
		if m == nil {
			continue
		}
		node.Value += p.Parse(m)
		p.Goto(m)
		p.Next()

		return node
	}

	return nil
}
