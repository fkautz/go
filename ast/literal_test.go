package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExampleLiteral() {

	// Literal <- DQ String DQ / Unicode / Binary / Hexadec / Octal
	p := parser.New()

	// "ABC"
	p.Init("\"ABC\"")
	ast.Literal(p).Print()

	// U+000F
	p.Init("U+000F")
	ast.Literal(p).Print()

	// b10
	p.Init("b10")
	ast.Literal(p).Print()

	// Output:
	// [37,"ABC"]
	// [40,"U+000F"]
	// [42,"b10"]

}
