package ast

import (
	pegn "gitlab.com/pegn/go"
	"gitlab.com/pegn/go/ast/nd"
)

// PosLook <-- '&' Rule
func PosLook(p pegn.Parser) *pegn.Node {

	node := new(pegn.Node)
	node.Type = nd.PosLook

	var n *pegn.Node
	beg := p.Mark()

	// '&'
	if p.Expect('&') == nil {
		p.Goto(beg)
		return nil
	}

	// Rule
	n = Rule(p)
	if n == nil {
		p.Goto(beg)
		return nil
	}
	node.AppendChild(n)

	return node
}
