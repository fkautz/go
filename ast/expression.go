package ast

import (
	pegn "gitlab.com/pegn/go"
	"gitlab.com/pegn/go/ast/nd"
	"gitlab.com/pegn/go/is"
)

// Expression <-- Sequence (Spacing '/' SP+ Sequence)*
func Expression(p pegn.Parser) *pegn.Node {

	node := new(pegn.Node)
	node.Type = nd.Expression

	var n *pegn.Node

	// Sequence
	n = Sequence(p)
	if n == nil {
		return nil
	}
	node.AppendChild(n)

	// (Spacing '/' SP+ Sequence)*
	for {

		// Spacing
		n = Spacing(p)
		if n == nil {
			break
		}
		node.AdoptFrom(n)

		if p.Expect('/', is.Min{' ', 1}) == nil {
			break
		}

		// Sequence
		n = Sequence(p)
		if n == nil {
			break
		}
		node.AppendChild(n)

	}

	return node
}
