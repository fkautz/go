package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExampleUniRange() {

	// UniRange <-- "[" Unicode "-" Unicode "]"
	p := parser.New()

	// [U+0000-U+00FF]
	p.Init("[U+0000-U+00FF]")
	ast.UniRange(p).Print()

	// [U+0000FF-U+00FFFF]
	p.Init("[U+0000FF-U+00FFFF]")
	ast.UniRange(p).Print()

	// Output:
	// [31,[[40,"U+0000"],[40,"U+00FF"]]]
	// [31,[[40,"U+0000FF"],[40,"U+00FFFF"]]]

}
