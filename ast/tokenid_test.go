package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExampleTokenId() {

	// TokenId <-- upper upper+
	p := parser.New()

	// TOKENID
	p.Init("TOKENID")
	ast.TokenId(p).Print()

	// Output:
	// [16,"TOKENID"]

}
