package ast

import (
	pegn "gitlab.com/pegn/go"
	"gitlab.com/pegn/go/ast/nd"
	"gitlab.com/pegn/go/is"
)

// ClassExpr <-- Simple (Spacing "/" SP+ Simple)*
func ClassExpr(p pegn.Parser) *pegn.Node {

	node := new(pegn.Node)
	node.Type = nd.ClassExpr

	var n *pegn.Node
	beg := p.Mark()

	// Simple
	n = Simple(p)
	if n == nil {
		p.Goto(beg)
		return nil
	}
	node.AppendChild(n)

	// (Spacing "/" SP+ Simple)*
	for {

		b := p.Mark()

		// Spacing
		n = Spacing(p)
		if n == nil {
			p.Goto(b)
			break
		}

		// "/" SP+
		if p.Expect('/', is.Min{' ', 1}) == nil {
			p.Goto(b)
			break
		}

		// Simple
		n = Simple(p)
		if n == nil {
			p.Goto(b)
			break
		}
		node.AppendChild(n)

	}

	return node
}
