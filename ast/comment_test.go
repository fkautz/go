package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExampleComment() {

	// Comment <-- (!EndLine any)+
	p := parser.New()

	//
	p.Init("\n")
	ast.Comment(p).Print()

	// SP
	p.Init(" ")
	ast.Comment(p).Print()

	// # C
	p.Init("# C")
	ast.Comment(p).Print()

	// Output:
	// <nil>
	// [4," "]
	// [4,"# C"]

}
