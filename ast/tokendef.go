package ast

import (
	pegn "gitlab.com/pegn/go"
	"gitlab.com/pegn/go/ast/nd"
	"gitlab.com/pegn/go/is"
)

// TokenDef <-- (ResTokenId / TokenId) SP+ "<-"  SP+ Literal ComEndLine
func TokenDef(p pegn.Parser) *pegn.Node {

	node := new(pegn.Node)
	node.Type = nd.TokenDef

	var n *pegn.Node

	// (ResTokenId / TokenId)
	for {

		// ResTokenId
		n = ResTokenId(p)
		if n != nil {
			break
		}

		// TokenId
		n = TokenId(p)
		if n != nil {
			break
		}

		return nil
	}
	node.AppendChild(n)

	// SP+ "<-" SP+
	if p.Expect(is.Min{' ', 1}, "<-", is.Min{' ', 1}) == nil {
		return nil
	}

	// Literal
	n = Literal(p)
	if n == nil {
		return nil
	}
	node.AppendChild(n)

	// ComEndLine
	n = ComEndLine(p)
	if n == nil {
		return nil
	}
	node.AdoptFrom(n)

	return node
}
