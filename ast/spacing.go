package ast

import (
	pegn "gitlab.com/pegn/go"
	"gitlab.com/pegn/go/is"
)

// Spacing <- ComEndLine? SP+
func Spacing(p pegn.Parser) *pegn.Node {

	node := new(pegn.Node)
	var n *pegn.Node

	// ComEndLine?
	n = ComEndLine(p)
	if n != nil {
		node.AdoptFrom(n)
	}

	// SP+
	if p.Expect(is.Min{' ', 1}) == nil {
		return nil
	}

	return node
}
