package ast

import (
	pegn "gitlab.com/pegn/go"
	"gitlab.com/pegn/go/ast/nd"
	"gitlab.com/pegn/go/is"
)

// Unicode <-- "U+" hexdig{4,8}
func Unicode(p pegn.Parser) *pegn.Node {

	node := new(pegn.Node)
	node.Type = nd.Unicode

	var m *pegn.Mark

	// "U+" upperhex{4,8}
	m = p.Check("U+", is.MinMax{is.UpperHex, 4, 8})
	if m == nil {
		return nil
	}
	node.Value += p.Parse(m)
	p.Goto(m)
	p.Next()

	return node
}
