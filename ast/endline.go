package ast

import (
	pegn "gitlab.com/pegn/go"
	"gitlab.com/pegn/go/ast/nd"
)

// EndLine <-- LF / CR LF / CR
func EndLine(p pegn.Parser) *pegn.Node {

	node := new(pegn.Node)
	node.Type = nd.EndLine

	var m *pegn.Mark

	// LF / CR LF / CR
	for {

		// LF
		m = p.Check('\n')
		if m != nil {
			break
		}

		// CR LF
		m = p.Check("\r\n")
		if m != nil {
			break
		}

		// CR
		m = p.Check('\r')
		if m != nil {
			break
		}

		return nil
	}

	node.Value += p.Parse(m)
	p.Goto(m)
	p.Next()

	return node
}
