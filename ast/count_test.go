package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExampleCount() {

	// Count <-- "{" digit+ "}"
	p := parser.New()

	// {1}
	p.Init("{1}")
	ast.Count(p).Print()

	// {99}
	p.Init("{99}")
	ast.Count(p).Print()

	// Output:
	// [30,"1"]
	// [30,"99"]

}
