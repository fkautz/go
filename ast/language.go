package ast

import (
	pegn "gitlab.com/pegn/go"
)

// Language <- Lang ("-" LangExt)?
func Language(p pegn.Parser) *pegn.Node {

	node := new(pegn.Node)

	var n *pegn.Node

	// Lang
	n = Lang(p)
	if n == nil {
		return nil
	}
	node.AppendChild(n)

	for {

		// "-"
		if p.Expect('-') == nil {
			break
		}

		// LangExt?
		n = LangExt(p)
		if n == nil {
			break
		}
		node.AppendChild(n)

		break
	}

	return node
}
