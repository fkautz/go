package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExampleResTokenId() {

	// ResTokenId <-- "TAB" / "LF" / "CR" / "CRLF" / "SP" / "VT" / "FF"
	//              / "NOT" / "BANG" / "DQ"
	//              / "HASH" / "DOLLAR" / "PERCENT" / "AND" / "SQ"
	//              / "LPAREN" / "RPAREN" / "STAR" / "PLUS" / "COMMA"
	//              / "DASH" / "MINUS" / "DOT" / "SLASH" / "COLON" / "SEMI"
	//              / "LT" / "EQ" / "GT" / "QUERY" / "QUESTION" / "AT"
	//              / "LBRAKT" / "BKSLASH" / "RBRAKT" / "CARET" / "UNDER"
	//              / "BKTICK" / "LCURLY" / "LBRACE" / "BAR" / "PIPE"
	//              / "RCULRY" / "RBRACE" / "TILDE" / "UNKNOWN" / "REPLACE"
	//              / "MAXRUNE" / "MAXASCII" / "MAXLATIN" / "LARROW"
	//              / "RARROW" / "LLARROW" / "RLARROW" / "LARROWF"
	//              / "LFAT" / "RARROWF" / "RFAT" / "WALRUS"
	p := parser.New()

	// LF
	// CR
	p.Init("LF CR")
	ast.ResTokenId(p).Print()
	ast.Spacing(p)
	ast.ResTokenId(p).Print()

	// Output:
	// [47,"LF"]
	// [47,"CR"]

}
