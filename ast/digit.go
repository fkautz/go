package ast

import (
	pegn "gitlab.com/pegn/go"
	"gitlab.com/pegn/go/ast/nd"
	"gitlab.com/pegn/go/is"
)

// Digit <-- digit
func Digit(p pegn.Parser) *pegn.Node {

	node := new(pegn.Node)
	node.Type = nd.Digit

	var m *pegn.Mark

	// digit
	m = p.Check(is.Digit)
	if m == nil {
		return nil
	}
	node.Value += p.Parse(m)
	p.Goto(m)
	p.Next()

	return node
}
