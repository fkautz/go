package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExampleClassId() {

	// ClassId <-- lower lower+
	p := parser.New()

	// classid
	// anotherid
	p.Init("classid anotherid")
	ast.ClassId(p).Print()
	ast.Spacing(p)
	ast.ClassId(p).Print()

	// Output:
	// [15,"classid"]
	// [15,"anotherid"]

}
