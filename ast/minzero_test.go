package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExampleMinZero() {

	// MinZero <-- "*"
	p := parser.New()

	// *
	p.Init("*")
	ast.MinZero(p).Print()

	// Output:
	// [25,"*"]

}
