package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExampleTokenDef() {

	// TokenDef <-- (ResTokenId / TokenId) SP+ "<-"  SP+ Literal ComEndLine
	p := parser.New()

	// TK <- " "\n
	p.Init("TK <- \" \"\n")
	ast.TokenDef(p).Print()

	// SP <- U+0000 # Comment\n
	p.Init("SP <- U+0000 # Comment\n")
	ast.TokenDef(p).Print()

	// Output:
	// [8,[[16,"TK"],[37," "],[45,"\n"]]]
	// [8,[[47,"SP"],[40,"U+0000"],[4,"# Comment"],[45,"\n"]]]

}
