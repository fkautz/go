package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExampleHexRange() {

	// HexRange <-- "[" Hexadec "-" Hexadec "]"
	p := parser.New()

	// [x0-xF]
	p.Init("[x0-xF]")
	ast.HexRange(p).Print()

	// [x00-xFF]
	p.Init("[x00-xFF]")
	ast.HexRange(p).Print()

	// Output:
	// [35,[[43,"x0"],[43,"xF"]]]
	// [35,[[43,"x00"],[43,"xFF"]]]

}
