package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExampleResClassId() {

	// ResClassId <-- "any" / "upperhex" / "lowerhex" / "alnum" / "alphanum"
	//              / "digit" / "punct" / "alpha" / "hexdig" / "octdig"
	//              / "bitdig" / "lower" / "upper" / "visible" / "quotable"
	//              / "ws" / "sign" / "control" / "ascii" / "blank" / "cntrl"
	//              / "graph" / "print" / "space" / "word" / "xdigit"
	p := parser.New()

	// any
	// ws
	p.Init("any ws")
	ast.ResClassId(p).Print()
	ast.Spacing(p)
	ast.ResClassId(p).Print()

	// Output:
	// [46,"any"]
	// [46,"ws"]

}
