package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExampleOctal() {

	// Octal <-- "o" octdig+
	p := parser.New()

	// o7
	p.Init("o7")
	ast.Octal(p).Print()

	// o007
	p.Init("o007")
	ast.Octal(p).Print()

	// Output:
	// [44,"o7"]
	// [44,"o007"]

}
