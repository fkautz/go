package ast

import (
	pegn "gitlab.com/pegn/go"
	"gitlab.com/pegn/go/is"
)

// ComEndLine <- SP* ("# " Comment)? EndLine
func ComEndLine(p pegn.Parser) *pegn.Node {

	node := new(pegn.Node)
	var n *pegn.Node
	beg := p.Mark()

	// SP*
	p.Expect(is.Min{' ', 1})

	// ("# " Comment)?
	for {

		b := p.Mark()

		// "# "
		if p.Check("# ") == nil {
			p.Goto(b)
			break
		}

		// Comment
		n = Comment(p)
		if n == nil {
			p.Goto(b)
			break
		}
		node.AppendChild(n)

		break
	}

	// EndLine
	n = EndLine(p)
	if n == nil {
		p.Goto(beg)
		return nil
	}
	node.AppendChild(n)

	return node
}
