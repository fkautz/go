package ast

import pegn "gitlab.com/pegn/go"

// Definition <- SchemaDef / CheckDef / ClassDef / TokenDef
func Definition(p pegn.Parser) *pegn.Node {

	if n := SchemaDef(p); n != nil {
		return n
	}

	if n := CheckDef(p); n != nil {
		return n
	}

	if n := ClassDef(p); n != nil {
		return n
	}

	if n := TokenDef(p); n != nil {
		return n
	}

	return nil
}
