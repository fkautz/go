package ast

import (
	pegn "gitlab.com/pegn/go"
	"gitlab.com/pegn/go/ast/nd"
	"gitlab.com/pegn/go/is"
)

// Home <-- (!ws any)+
func Home(p pegn.Parser) *pegn.Node {

	node := new(pegn.Node)
	node.Type = nd.Home

	var m *pegn.Mark

	// (!ws any)+
	m = p.Check(is.Min{is.Seq{is.Not{is.WhiteSpace}, is.Any}, 1})
	if m == nil {
		return nil
	}
	node.Value = p.Parse(m)
	p.Goto(m)
	p.Next()

	return node
}
