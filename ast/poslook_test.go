package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExamplePosLook() {

	// PosLook <-- '&' Rule
	p := parser.New()

	// &Rule
	p.Init("&Rule")
	ast.PosLook(p).Print()

	// Output:
	// [20,[[22,[[14,"Rule"]]]]]

}
