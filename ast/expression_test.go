package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExampleExpression() {

	// Expression <-- Sequence (Spacing '/' SP+ Sequence)*
	p := parser.New()

	// Rule / Another
	p.Init("Rule / Another")
	ast.Expression(p).Print()

	// Output:
	// [17,[[19,[[22,[[14,"Rule"]]]]],[19,[[22,[[14,"Another"]]]]]]]

}
