package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExampleDigit() {

	// Digit <-- digit
	p := parser.New()

	// 0
	p.Init("0")
	ast.Digit(p).Print()

	// 9
	p.Init("9")
	ast.Digit(p).Print()

	// Output:
	// [39,"0"]
	// [39,"9"]

}
