package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExampleHexadec() {

	// Hexadec <-- "x" upperhex+
	p := parser.New()

	// x0A
	p.Init("x0A")
	ast.Hexadec(p).Print()

	// xAAA
	p.Init("xAAA")
	ast.Hexadec(p).Print()

	// Output:
	// [43,"x0A"]
	// [43,"xAAA"]

}
