package ast

import (
	pegn "gitlab.com/pegn/go"
	"gitlab.com/pegn/go/ast/nd"
	"gitlab.com/pegn/go/is"
)

// Min <-- digit+
func Min(p pegn.Parser) *pegn.Node {

	node := new(pegn.Node)
	node.Type = nd.Min

	var m *pegn.Mark

	// digit+
	m = p.Check(is.Min{is.Digit, 1})
	if m == nil {
		return nil
	}
	node.Value += p.Parse(m)
	p.Goto(m)
	p.Next()

	return node
}
