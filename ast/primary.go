package ast

import (
	pegn "gitlab.com/pegn/go"
)

// Primary <-- Simple / CheckId / '(' Expression ')'
func Primary(p pegn.Parser) *pegn.Node {

	var node *pegn.Node

	// Primary
	node = Simple(p)
	if node != nil {
		return node
	}

	// CheckId
	node = CheckId(p)
	if node != nil {
		return node
	}

	// '('
	if p.Expect('(') == nil {
		return nil
	}

	// Expression
	node = Expression(p)
	if node == nil {
		return nil
	}

	// ')'
	if p.Expect(')') == nil {
		return nil
	}

	return node
}
