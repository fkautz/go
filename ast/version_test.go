package ast_test

import (
	"gitlab.com/pegn/go/ast"
	"gitlab.com/pegn/go/parser"
)

func ExampleVersion() {

	// Version <- 'v' MajorVer '.' MinorVer '.' PatchVer
	p := parser.New()

	// v0.31.1
	p.Init("v0.31.1")
	ast.Version(p).Print()

	// Output:
	// [0,[[11,"0"],[12,"31"],[13,"1"]]]

}
